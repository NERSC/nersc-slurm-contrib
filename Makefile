## configuration
GIT_REVISION=$(shell git rev-parse --short=8 HEAD)
BASE=$(shell basename $(PWD))

dist:
	cat nersc-slurm-contrib.spec.in | sed 's|@@GIT_REVISION@@|$(GIT_REVISION)|g' > nersc-slurm-contrib.spec
	cd .. && tar czf /tmp/nersc-slurm-contrib-$(GIT_REVISION).tar.gz --exclude=.git --exclude=*.tar.bz2 --exclude=*.tar.gz --exclude=*.rpm --exclude=workdir* "$(BASE)"
	mv /tmp/nersc-slurm-contrib-$(GIT_REVISION).tar.gz .
