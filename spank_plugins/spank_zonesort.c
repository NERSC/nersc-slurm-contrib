/*
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <numa.h>

#include <slurm/spank.h>

SPANK_PLUGIN(zonesort, 1)

#define ZS_NO_ERROR 0
#define ZS_CPUSET_FIND_FAILURE 1
#define ZS_CPUSET_OPEN_FAILURE 2
#define ZS_SPANK_FAILURE 3
#define ZS_CPUSET_PARSE_FAILURE 4
#define ZS_ZSKMOD_FAILURE 5
#define ZS_WRITE_FAILURE 6

static const char *error_messages[] = {
    "No error",
    "Could not find job cpuset",
    "Could not open job cpuset",
    "General failure in setup",
    "Could not parse cpuset numa nodes",
    "zonesort kernel module not found",
    "zonesort interface write failure"
};

static int zonesort_status = 0;
static int do_disable_zonesort = 0;
static unsigned int zonesort_interval = 0;
static char *zonesort_option = NULL;

int zonesort_option_handler(int val, const char *optarg, int remote);
struct spank_option spank_option_array[] = {
    { "zonesort", "on|off|<interval>", "[NERSC Option] perform Intel XPPS "
      "Zonesort operation. \"on\" runs zonesort once prior to each step "
      "(default); \"off\" disables zonesort; a positive integer sets the "
      "sorting interval to be used (and implies \"on\")", 1, 0,
      zonesort_option_handler
    },
    SPANK_OPTIONS_TABLE_END
};

int zonesort_option_handler(int vali, const char *optarg, int remote) {
    if (optarg == NULL) return ESPANK_ERROR;
    if (strlen(optarg) > 100) return ESPANK_ERROR;
    zonesort_option = strdup(optarg);
    if (strcasecmp(optarg, "on") == 0 || strcasecmp(optarg, "yes") == 0 || strcasecmp(optarg, "true") == 0) {
        do_disable_zonesort = 0;
        zonesort_interval = 0;
        return ESPANK_SUCCESS;
    }
    if (strcasecmp(optarg, "off") == 0 || strcasecmp(optarg, "no") == 0 || strcasecmp(optarg, "false") == 0) {
        do_disable_zonesort = 1;
        zonesort_interval = 0;
	return ESPANK_SUCCESS;
    }
    /* only accept zero or positive integers */
    if (!isdigit(optarg[0])) return ESPANK_ERROR;
    do_disable_zonesort = 0;
    zonesort_interval = atoi(optarg);

    return ESPANK_SUCCESS;
}

int setup_zonesort_from_jobenv(char **env) {
    char **ptr = env;
    for ( ; ptr && *ptr; ptr++) {
        if (strncmp(*ptr, "SLURM_SPANK_NERSC_ZONESORT_INTERVAL=", 36) == 0) {
            zonesort_interval = (unsigned int) strtoul(*ptr + 36, NULL, 10);
        } else if (strncmp(*ptr, "SLURM_SPANK_NERSC_ZONESORT_DISABLE=", 35) == 0) {
            do_disable_zonesort = atoi(*ptr + 35);
        }
    }
    slurm_info("zonesort: zonesort_interval=%u, do_disable_zonesort=%d", zonesort_interval, do_disable_zonesort);
    return ESPANK_SUCCESS;
}

int perform_zonesort(spank_t sp, int argc, char **argv) {
    int rc = ESPANK_SUCCESS;
    char *buffer = NULL;
    char *filename = NULL;
    FILE *fp = NULL;

    uint32_t job_id;
    uint32_t step_id;
    uid_t uid;

    zonesort_status = 0;
    buffer = (char *) malloc(sizeof(char) * 2048);
    filename = NULL;
    fp = NULL;

    if (buffer == NULL) {
        slurm_error("zonesort: FAILED: buffer not allocated!");
        zonesort_status = ZS_SPANK_FAILURE;
        rc = ESPANK_ERROR;
        goto terminate;
    }

    rc = spank_get_item(sp, S_JOB_UID, &uid);
    if (rc != ESPANK_SUCCESS) {
        zonesort_status = ZS_SPANK_FAILURE;
        goto terminate;
    }
    rc = spank_get_item(sp, S_JOB_ID, &job_id);
    if (rc != ESPANK_SUCCESS) {
        zonesort_status = ZS_SPANK_FAILURE;
        goto terminate;
    }
    rc = spank_get_item(sp, S_JOB_STEPID, &step_id);
    if (rc != ESPANK_SUCCESS) {
        zonesort_status = ZS_SPANK_FAILURE;
        goto terminate;
    }
    
    int signed_stepid = (int) step_id;
    if (signed_stepid < 0) {
        /* ignore batch and extern steps */
        rc = ESPANK_SUCCESS;
        goto terminate;
    }

    /* find the cpuset cgroup we're using */
    /* try the new variant from SP32 first before falling back to the old name */
    if (asprintf(&filename, "/dev/cpuset/slurm/uid_%d/job_%d/cpuset.mems", uid, job_id) < 0) {
        slurm_error("zonesort: FAILED to generate cpuset filename!");
        zonesort_status = ZS_CPUSET_FIND_FAILURE;
        rc = ESPANK_ERROR;
        goto terminate;
    }
    if( -1 == access(filename, F_OK) ) {
        /* The new path does not exist, fall back to the old one */
        if (asprintf(&filename, "/dev/cpuset/slurm/uid_%d/job_%d/mems", uid, job_id) < 0) {
            slurm_error("zonesort: FAILED to generate cpuset filename!");
            zonesort_status = ZS_CPUSET_FIND_FAILURE;
            rc = ESPANK_ERROR;
            goto terminate;
        }
    }
    fp = fopen(filename, "r");
    free(filename);
    filename = NULL;
    if (fp == NULL) {
	/* Warn that we couldn't open the job's mems file */
        slurm_error("zonesort: could not open job cpuset mems, falling back to global one for job %d", job_id);
        /* Check the SP32 variant exists, otherwise fall back */
        if ( access("/dev/cpuset/cpuset.mems", F_OK) )
            fp = fopen("/dev/cpuset/cpuset.mems", "r");
        else
            fp = fopen("/dev/cpuset/mems", "r");
        /* Did it work? */
        if (fp == NULL) {
            slurm_error("zonesort: FAILED to open cpuset mems for job %d", job_id);
            zonesort_status = ZS_CPUSET_OPEN_FAILURE;
            rc = ESPANK_ERROR;
            goto terminate;
        }
    }

    /* parse which numa nodes the job has access to */
    struct bitmask *numa_bm = NULL;
    size_t nread = 0;
    char *eol_ptr = NULL;

    nread = fread(buffer, sizeof(char), 2048, fp);
    fclose(fp);
    fp = NULL;
    if (nread > 2047) {
        slurm_error("zonesort: FAILED: read from cpuset mems too long!");
        zonesort_status = ZS_CPUSET_PARSE_FAILURE;
        rc =  ESPANK_ERROR;
        goto terminate;
    }
    buffer[nread] = '\0';
    eol_ptr = strchr(buffer, '\n');
    if (eol_ptr != NULL) {
        *eol_ptr = '\0';
    }

    numa_bm = numa_parse_nodestring(buffer);
    if (numa_bm == NULL) {
        slurm_error("zonesort: FAILED to parse numa nodestring: %s!", buffer);
        zonesort_status = ZS_CPUSET_PARSE_FAILURE;
        rc = ESPANK_ERROR;
        goto terminate;
    }

    /* set zonesort interval */
    int fd = open("/sys/kernel/zone_sort_free_pages/sort_interval", O_WRONLY);
    if (fd == -1) {
        slurm_error("zonesort: failed to open interval file for zonesort");
        zonesort_status = ZS_ZSKMOD_FAILURE;
        rc = ESPANK_SUCCESS;
        goto terminate;
    }
    char write_buffer[128];
    snprintf(write_buffer, 128, "%u\n", zonesort_interval);
    errno = 0;
    ssize_t ret = write(fd, write_buffer, strlen(write_buffer));
    slurm_info("zonesort: set sort interval to %u seconds (%ld bytes written), %s", zonesort_interval, ret, strerror(errno));
    close(fd);
    if (ret < 0) {
        zonesort_status = ZS_WRITE_FAILURE;
    }
    
    /* run zone sort on just those numa domains */
    if (!do_disable_zonesort && zonesort_interval == 0) {
        /* initiate memory compaction */
        fd = open("/proc/sys/vm/compact_memory", O_WRONLY);
        if (fd == -1) {
            zonesort_status = ZS_ZSKMOD_FAILURE;
            slurm_error("zonesort: failed to open memory compaction file");
            goto terminate;
        }
        snprintf(write_buffer, 128, "1\n");
        ret = write(fd, write_buffer, strlen(write_buffer));
        slurm_info("zonesort: initiated memory compaction (%ld bytes written), %s", ret, strerror(errno));
        close(fd);
        if (ret < 0) {
            zonesort_status = ZS_WRITE_FAILURE;
        }
        int numa_cnt = numa_bitmask_weight(numa_bm);
        slurm_info("zonesort: found %d numa domains", numa_cnt);
        int idx = 0;
        for (idx = 0; idx < numa_cnt; idx++) {
            if (*(numa_bm->maskp) & ((long unsigned) 1 << idx)) {
                ret = 0;
                slurm_info("zonesort: will perform zonesort for numa node %d", idx);
                fd = open("/sys/kernel/zone_sort_free_pages/nodeid", O_WRONLY);
                if (fd == -1) {
                    slurm_error("zonesort: failed to open file for zonesort, %d", idx);
                    zonesort_status = ZS_ZSKMOD_FAILURE;
                    continue;
                }
                snprintf(write_buffer, 128, "%d\n", idx);
                errno = 0;
                ret = write(fd, write_buffer, strlen(write_buffer));
                slurm_info("zonesort: wrote %ld bytes to zone_sort_free_pages: %s", ret, strerror(errno));
                close(fd);
                if (ret < 0) {
                    zonesort_status = ZS_WRITE_FAILURE;
                }
            }
        }
    }

    numa_bitmask_free(numa_bm);

terminate:
    if (buffer != NULL) {
        free(buffer);
        buffer = NULL;
    }
    if (filename != NULL) {
        free(filename);
        filename = NULL;
    }
    if (fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    return rc;
}

int slurm_spank_init(spank_t sp, int argc, char **argv) {
    spank_context_t context;
    int rc = ESPANK_SUCCESS;
    int idx = 0;

    context = spank_context();

    if (context == S_CTX_ALLOCATOR || context == S_CTX_LOCAL || context == S_CTX_REMOTE) {
        for (idx = 0; spank_option_array[idx].name != NULL; idx++) {
            spank_option_register(sp, &(spank_option_array[idx]));
        }
    }
    return rc;
}

int slurm_spank_init_post_opt(spank_t sp, int argc, char **argv) {
    spank_context_t context;
    context = spank_context();
    char buffer[128];
    int rc = ESPANK_SUCCESS;
    if (do_disable_zonesort) {
        if (context == S_CTX_ALLOCATOR) {
            spank_job_control_setenv(sp, "NERSC_ZONESORT_DISABLE", "1", 1);
        } else if (context == S_CTX_LOCAL) {
            spank_setenv(sp, "NERSC_ZONESORT_DISABLE", "1", 1);
        }
    }
    snprintf(buffer, 128, "%u", zonesort_interval);
    if (context == S_CTX_ALLOCATOR) {
        spank_job_control_setenv(sp, "NERSC_ZONESORT_INTERVAL", buffer, 1);
    } else if (context == S_CTX_LOCAL) {
        spank_setenv(sp, "NERSC_ZONESORT_INTERVAL", buffer, 1);
    
    }
    /* this persists the user's option */
    spank_setenv(sp, "_SLURM_SPANK_OPTION_zonesort_zonesort", zonesort_option, 1);

    if (context == S_CTX_REMOTE) {
        if (access("/sys/kernel/zone_sort_free_pages/nodeid", W_OK) == 0) {
            rc = perform_zonesort(sp, argc, argv);
        }
    }

    return rc;
}

int slurm_spank_task_init(spank_t sp, int argc, char **argv) {
    if (zonesort_status != 0) {
        const char *errmsg = error_messages[zonesort_status];
        uint32_t job_id = 0;
        uint32_t step_id = 0;

        spank_get_item(sp, S_JOB_ID, &job_id);
        spank_get_item(sp, S_JOB_STEPID, &step_id);
        slurm_error("Detected zonesort setup failure: %s (%d.%d)", errmsg, job_id, step_id);
    }
    return ESPANK_SUCCESS;
}
