#!/bin/bash
set -e

## pause ldms syspapi data collection
echo -n pause | /opt/ovis/sbin/ldmsd_stream_publish -a munge -x sock -h localhost -p 410 -s syspapi_stream -t string

## allow uncore examination
echo 0 > /proc/sys/kernel/perf_event_paranoid

## allow kernel pointers to be written unrestricted
echo 0 > /proc/sys/kernel/kptr_restrict
