/*
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <dlfcn.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <limits.h>
#include <pwd.h>
#include <grp.h>
#include <munge.h>
#include <slurm/spank.h>
#include <slurm/slurm.h>
#include "utility.h"

#ifndef TEST_BUILD

SPANK_PLUGIN(nerscperf, 1)
#else
#include <stdarg.h>
#define ESPANK_ERROR 1
#define ESPANK_SUCCESS 0

void slurm_error(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vprintf(fmt, ap);
    va_end(ap);
    printf("\n");
}
spank_context_t spank_context() {
    return S_CTX_LOCAL;
}
spank_err_t spank_option_register (spank_t spank, struct spank_option *opt) {
    return ESPANK_SUCCESS;
}

#endif

/* example config in plugstack.conf
required /path/to/nerscPerf.so base_start=/path/to/basestart.sh base_stop=/path/to/basestop.sh \
    modules=vtune,msrsafe \
    vtune_versions=2017up1,2017up1_emon \
    vtune_default=2017up1 \
    vtune_start_2017up1=/path/to/start.sh \       ## path to startup script, included with kmod in image
    vtune_stop_2017up1=/path/to/stop.sh \         ## path to stop script, included with kmod in image
    vtune_module_2017up1=vtune/blah \             ## module that user should have loaded in srun (to generate error if mismatch between kernel and userspace) (OPTIONAL)
    vtune_start_2017up1_emon=/path/to/start.sh \  ## path to startup script, included with kmod in image
    vtune_stop_2017up1_emon=/path/to/stop.sh \    ## path to stop script, included with kmod in image
    vtune_access_2017up1_emon=u:dmj,g:nstaff \    ## ACLs for dangerous new bleeding edge things which obviously belong on severed arm computers
    msrsafe_versions=base \
    msrsafe_default=base \
    msrsafe_start_base=/path/to/start.sh \
    msrsafe_stop_base=/path/to/stop.sh
    msrsafe_incompatible_base=vtune,other/thing1
*/

static void *_realloc(void *ptr, size_t alloc_size) {
    void *tmp = realloc(ptr, alloc_size);
    if (tmp == NULL) {
        slurm_error("nersc_perf: failed to allocate memory");
        abort();
    }
    return tmp;
}

static char *_strdup(const char *str) {
    char *tmp = strdup(str);
    if (tmp == NULL) {
        slurm_error("nersc_perf: failed to allocate memory");
        abort();
    }
    return tmp;
}

static void *_malloc(size_t alloc_size) {
    void *tmp = malloc(alloc_size);
    if (tmp == NULL) {
        slurm_error("nersc_perf: failed to allocate memory");
        abort();
    }
    return tmp;
}

struct perf_module_config;
struct perf_module_instance;

struct perf_module {
    struct perf_module_config *module;
    struct perf_module_instance *version;
};

struct perf_module_instance {
    char *version;
    char *start;
    char *stop;
    char *module;
    int *uids;
    int n_uids;
    int *gids;
    int n_gids;
    struct perf_module **incompatible;
    int n_incompatible;
};

struct perf_module_config {
    char *module_name;
    char *default_version;
    struct perf_module_instance **instances;
    size_t n_instances;
};

struct perf_config {
    struct perf_module_config **modules;
    size_t n_modules;
    char *base_start;
    char *base_stop;
};

enum perf_errno {
    PERF_SUCCESS = 0,
    PERF_ERROR,
    PERF_NO_SHARED,
    PERF_NO_PERMISSIONS,
    PERF_INVALID_SCRIPT,
    PERF_INVALID
};

const char *perf_status_str[] = {
    "Success, no error",
    "Unspecified error",
    "Perf modules cannot be used in shared allocations",
    "Invalid permissions for selected perf modules",
    "Invalid script",
    "Invalid error, should not see this",
    NULL
};

struct script_set {
    char **script_paths;
    char **modules;
    int n_scripts;
    size_t script_sz;
    int *exit_status;
};

struct perf_status {
    struct script_set prologs;
    struct script_set epilogs;
};

int _perf_option_handler(int val, const char *optarg, int remote);

static struct spank_option perf_option = { NULL, NULL, NULL, -1, -1, NULL };
static struct perf_config *config = NULL;
static char *effective_perf_option_optarg = NULL;
static char *cache_hostname = NULL;
static char *cache_kernel_release = NULL;
static char *cache_percent = NULL;

static struct perf_module *user_requested_modules = NULL;
static int n_user_requested_modules = 0;
static int ran_perf_option_handler = 0;
static void *libslurm_handle = NULL;
static struct perf_status status = { {NULL,NULL,0,0,NULL}, {NULL,NULL,0,0,NULL}};
static int error_status = 0;
int validate_script(const char *script, const char *module_name, const char *script_type);
int _read_perf_status_file(struct perf_status *status, uint32_t job_id, int *error_status);
int _write_perf_status_file(struct perf_status *status, uint32_t job_id, int error_status);

int allocate_script_set(struct script_set *set, int n) {
    int idx = 0;
    set->script_paths = _malloc(sizeof(char *) * n);
    set->modules = _malloc(sizeof(char *) * n);
    set->exit_status = _malloc(sizeof(int) * n);
    for (idx = 0; idx < n; idx++) {
        set->script_paths[idx] = NULL;
        set->modules[idx] = NULL;
        set->exit_status[idx] = -1;
    }
    set->script_sz = n;
    set->n_scripts = n;
    return PERF_SUCCESS;
}

int add_script_to_set(struct script_set *set, const char *script, const char *module_name) {
    while (set->n_scripts + 1 >= set->script_sz) {
        size_t capacity = set->script_sz + 10;
        set->script_paths = _realloc(set->script_paths, sizeof(char *) * capacity);
        set->modules = _realloc(set->modules, sizeof(char *) * capacity);
        set->exit_status = _realloc(set->exit_status, sizeof(int) * capacity);
        set->script_sz = capacity;
    }
    int idx = set->n_scripts;
    slurm_debug("nersc_perf: adding script %s (%d) for %s", script, idx, module_name);
    set->script_paths[idx] = _strdup(script);
    set->modules[idx] = _strdup(module_name);
    set->exit_status[idx] = -1;
    set->n_scripts++;
    return PERF_SUCCESS;
}

char *_determine_hostname(spank_t sp) {
    char *hostbuf = _malloc(sizeof(char *) * 128);
    if (gethostname(hostbuf, 128) == 0) {
        return hostbuf;
    }
    free(hostbuf);
    return NULL;
}
char *_determine_kernel_release(spank_t sp) {
    struct utsname uts;
    memset(&uts, 0, sizeof(struct utsname));
    if (uname(&uts) == 0) {
        return _strdup(uts.release);
    }
    return NULL;
}
char *_set_percent(spank_t sp) {
    return _strdup("%");
}

void replace_in_path(char **path, char **wptr, size_t replace_len,
        const char *replace, char **end_ptr, size_t *path_sz)
{
    char *ret = NULL;
    char *before = *path;
    char *after = *wptr + replace_len;
    size_t final_len = 0;
    **wptr = '\0';

    final_len = strlen(before) + strlen(after) + strlen(replace) + 2;
    ret = _malloc(sizeof(char) * final_len);
    snprintf(ret, final_len, "%s%s%s", before, replace, after);
    *wptr = ret + (*wptr - *path);
    *path_sz = strlen(ret);
    *end_ptr = ret + *path_sz;
    *path = ret;
}

int evaluate_script_path(spank_t sp, const char *origpath, char **newpath, size_t *newpath_sz) {
    /* replace some formatting strings with discovered values:
            %N -- node hostname
            %r -- output of $(uname -r)
            %% -- percent character
    */
    char *ret = _strdup(origpath);
    size_t ret_sz = strlen(ret);
    char *ptr = ret;
    char *eptr = ret + ret_sz;
    int idx = 0;
    const char *fmt_str[] = {
        "%N", "%r", "%%", NULL
    };
    char **cache_strings[] = {
        &cache_hostname, &cache_kernel_release, &cache_percent, NULL
    };
    char *(*fmt_cache_fxns[])(spank_t) = {
        _determine_hostname,
        _determine_kernel_release,
        _set_percent,
        NULL
    };

    for (idx = 0; fmt_str[idx]; idx++) {
        const char *fmt = fmt_str[idx];
        char **cache_str = cache_strings[idx];
        char *(*fmt_cache_fxn)(spank_t) = fmt_cache_fxns[idx];

        ptr = ret;
        while (ptr < eptr) {
            ptr = strstr(ptr, fmt);
            if (ptr == NULL) break;
            if (*cache_str == NULL) {
                *cache_str = fmt_cache_fxn(sp);
            }
            replace_in_path(&ret, &ptr, strlen(fmt), *cache_str, &eptr, &ret_sz);
            ptr++;
        }
    }
    *newpath = ret;
    *newpath_sz = ret_sz;
    return PERF_SUCCESS;
}

int add_prolog(spank_t sp, struct perf_status *status, const char *script, char *module_name) {
    char *dereferenced_script = NULL;
    size_t dereferenced_script_sz = 0;
    int rc = PERF_SUCCESS;
    slurm_debug("nersc_perf: raw prolog path: %s", script);
    if (evaluate_script_path(sp, script, &dereferenced_script, &dereferenced_script_sz) != PERF_SUCCESS) {
        rc = PERF_INVALID_SCRIPT;
        goto terminate;
    }
    slurm_debug("nersc_perf: evaluated prolog path to %s", dereferenced_script);
    if (validate_script(dereferenced_script, module_name, "prolog") != ESPANK_SUCCESS) {
        rc = PERF_INVALID_SCRIPT;
        goto terminate;
    }
    rc = add_script_to_set(&(status->prologs), dereferenced_script, module_name);
terminate:
    if (dereferenced_script != NULL) {
        free(dereferenced_script);
        dereferenced_script = NULL;
    }
    return rc;
}

int add_epilog(spank_t sp, struct perf_status *status, const char *script, char *module_name) {
    char *dereferenced_script = NULL;
    size_t dereferenced_script_sz = 0;
    int rc = PERF_SUCCESS;
    slurm_debug("nersc_perf: raw epilog path: %s", script);
    if (evaluate_script_path(sp, script, &dereferenced_script, &dereferenced_script_sz) != PERF_SUCCESS) {
        rc = PERF_INVALID_SCRIPT;
        goto terminate;
    }
    slurm_debug("nersc_perf: evaluated epilog path to %s", dereferenced_script);
    if (validate_script(dereferenced_script, module_name, "epilog") != ESPANK_SUCCESS) {
       rc = PERF_INVALID_SCRIPT;
       goto terminate;
    }
    rc = add_script_to_set(&(status->epilogs), dereferenced_script, module_name);
terminate:
    if (dereferenced_script != NULL) {
        free(dereferenced_script);
        dereferenced_script = NULL;
    }
    return rc;
}

ssize_t parse_csv_list(char *value, char ***list) {
    ssize_t n_values = 0;
    if (value == NULL || list == NULL) return 0;
    char *start = NULL;
    char *end = NULL;
    char *ptr = NULL;

    start = value;
    ptr = start;
    end = value + strlen(value);
    while (start < end) {
        ptr = strchr(ptr, ',');
        if (ptr == NULL) {
            ptr = end;
        }
        *ptr = '\0';
        *list = (char **) _realloc(*list, sizeof(char *) * (n_values + 1));
        (*list)[n_values] = _strdup(start);
        n_values++;
        start = ptr + 1;
        ptr = start;
    }
    return n_values;
}


struct perf_module_config *lookup_module(struct perf_config *config,
        const char *module_name)
{
    struct perf_module_config *modconf = NULL;
    size_t idx = 0;
    for (idx = 0; idx < config->n_modules; idx++) {
        if (strcmp(config->modules[idx]->module_name, module_name) == 0) {
            modconf = config->modules[idx];
            break;
        }
    }
    return modconf;
}

struct perf_module_instance *lookup_module_version(
        struct perf_module_config *modconf, const char *version_str)
{
    struct perf_module_instance *version = NULL;
    size_t idx = 0;
    if (version_str == NULL) return NULL;
    if (modconf == NULL) return NULL;

    for (idx = 0; idx < modconf->n_instances; idx++) {
        if (strcasecmp(modconf->instances[idx]->version, version_str) == 0) {
            version = modconf->instances[idx];
            break;
        }
    }
    return version;
}

int parse_module(struct perf_config *config, char *key, char *value) {
    char *module_name = NULL;
    char *type = NULL;
    char *version_str = NULL;
    char *local_key = _strdup(key);
    char *key_end = local_key + strlen(local_key);
    struct perf_module_instance *version = NULL;

    char **args[] = { &module_name, &type, &version_str, NULL };
    char ***ptr = args;
    char *sptr = local_key;
    int rc = ESPANK_SUCCESS;

    while (ptr && *ptr && sptr < key_end) {
       char ***next = ptr + 1;
       char *end = NULL;

       if (*next != NULL) {
           end = strchr(sptr, '_');
       }
       if (end == NULL) end = key_end;
       *end = '\0';
       **ptr = _strdup(sptr);
       sptr = end + 1;
       ptr = ptr + 1;
    }

    if (module_name == NULL || type == NULL) {
        slurm_error("Failed to parse %s into a valid module name and type (and "
                "version, possibly)", key);
        rc = ESPANK_ERROR;
        goto terminate;
    }

    struct perf_module_config *modconf = lookup_module(config, module_name);
    if (modconf == NULL) {
        slurm_error("Invalid perf module: %s", module_name);
        rc = ESPANK_ERROR;
        goto terminate;
    }

    version = lookup_module_version(modconf, version_str);
    if (version_str != NULL && version == NULL) {
        slurm_error("Invalid perf module version: %s (for %s)",
                version_str, module_name);
    }

    if (strcasecmp(type, "versions") == 0 && version == NULL) {
        char **versions = NULL;
        ssize_t versions_count = 0;
        ssize_t ver_idx = 0;
        versions_count = parse_csv_list(value, &versions);

        for (ver_idx = 0; ver_idx < versions_count; ver_idx++) {
            struct perf_module_instance *inst = (struct perf_module_instance *)
                    _malloc(sizeof(struct perf_module_instance));
            memset(inst, 0, sizeof(struct perf_module_instance));

            /* transfer responsibility of allocated memory to inst */
            inst->version = versions[ver_idx];

            size_t alloc_size = sizeof(struct perf_module_instance *)
                    * (modconf->n_instances + 1);
            modconf->instances = (struct perf_module_instance **)
                    _realloc(modconf->instances, alloc_size);
            modconf->instances[modconf->n_instances] = inst;
            modconf->n_instances++;
        }
        if (versions != NULL) {
            free(versions);
        }

        /* by default set default version to the first version available */
        if (modconf->n_instances > 0 && modconf->default_version == NULL) {
            modconf->default_version = _strdup(modconf->instances[0]->version);
        }
    } else if (strcasecmp(type, "default") == 0 && version == NULL) {
        if (modconf->default_version != NULL) {
            free(modconf->default_version);
        }
        modconf->default_version = _strdup(value);
    } else if (strcasecmp(type, "start") == 0 && version != NULL) {
        if (version->start != NULL) {
            free(version->start);
        }
        version->start = _strdup(value);
    } else if (strcasecmp(type, "stop") == 0 && version != NULL) {
        if (version->stop != NULL) {
            free(version->stop);
        }
        version->stop = _strdup(value);
    } else if (strcasecmp(type, "module") == 0 && version != NULL) {
        if (version->module != NULL) {
            free(version->module);
        }
        version->module = _strdup(value);
    } else if (strcasecmp(type, "access") == 0 && version != NULL) {
        char **acls = NULL;
        ssize_t acls_count = 0;
        ssize_t acl_idx = 0;

        acls_count = parse_csv_list(value, &acls);
        for (acl_idx = 0; acl_idx < acls_count; acl_idx++) {
            char *lval = strchr(acls[acl_idx], ':');
            size_t lval_len = 0;
            int all_digits = 0;
            int idx = 0;
            if (lval == NULL) {
                continue;
            }
            *lval = '\0';
            lval++;
            lval_len = strlen(lval);
            idx = 0;
            while (idx < lval_len) {
                if (!isdigit(lval[idx])) break;
                idx++;
            }
            if (idx == lval_len) all_digits = 1;
            if (acls[acl_idx][0] == 'u') {
                uid_t uid = -1;
                if (all_digits) {
                    uid = atoi(lval);
                } else {
                    struct passwd *pw = getpwnam(lval);
                    if (pw != NULL) {
                        uid = pw->pw_uid;
                    }
                }
                if (uid >= 0) {
                    size_t alloc_size = sizeof(int) * (version->n_uids + 1);
                    version->uids = (int *) _realloc(version->uids, alloc_size);
                    version->uids[version->n_uids] = uid;
                    version->n_uids++;
                }
            } else if (acls[acl_idx][0] == 'g') {
                gid_t gid = -1;
                if (all_digits) {
                    gid = atoi(lval);
                } else {
                    struct group *grp = getgrnam(lval);
                    if (grp != NULL) {
                        gid = grp->gr_gid;
                    }

                }
                if (gid >= 0) {
                    size_t alloc_size = sizeof(int) * (version->n_gids + 1);
                    version->gids = (int *) _realloc(version->gids, alloc_size);
                    version->gids[version->n_gids] = gid;
                    version->n_gids++;
                }
            }
        }
    } else if (strcasecmp(type, "incompatible") == 0 && version != NULL) {
        char **incomp = NULL;
        ssize_t incomp_count = 0;
        ssize_t incomp_idx = 0;

        incomp_count = parse_csv_list(value, &incomp);
        for (incomp_idx = 0; incomp_idx < incomp_count; incomp_idx++) {
            char *lmod_name = incomp[incomp_idx];
            char *rptr = strchr(lmod_name, '_');
            if (rptr != NULL) {
                *rptr = '\0';
                rptr++;
            }

            struct perf_module_config *incompat_module = lookup_module(config, lmod_name);
            struct perf_module_instance *incompat_version = lookup_module_version(incompat_module, rptr);

            if (incompat_module == NULL) {
                slurm_error("Invalid module name \"%s\" in incompatbility list for %s_%s",
                    lmod_name, module_name, version_str);
                rc = ESPANK_ERROR;
                goto terminate;
            }
            if (incompat_version == NULL && rptr != NULL) {
                slurm_error("Invalid module version \"%s\" for module \"%s\"",
                    rptr, lmod_name);
                rc = ESPANK_ERROR;
                goto terminate;
            }

            struct perf_module *incompat = (struct perf_module *) _malloc(sizeof(struct perf_module));
            incompat->module = incompat_module;
            incompat->version = incompat_version;

            version->incompatible = (struct perf_module **) _realloc(
                    version->incompatible,
                    sizeof(struct perf_module *) * (version->n_incompatible + 1)
            );
            version->incompatible[version->n_incompatible] = incompat;
            version->n_incompatible++;
        }
    }
terminate:
    if (local_key != NULL) {
        free(local_key);
        local_key = NULL;
    }
    return rc;
}

void _dlopen_libslurm(void) {
    if (libslurm_handle != NULL) return;
    char buffer[1024];
    snprintf(buffer, 1024, "libslurm.so");
    libslurm_handle = dlopen(buffer, RTLD_NOW | RTLD_GLOBAL);
}

int get_job_attributes(uint32_t jobid, uint16_t *shared) {
    _dlopen_libslurm();
    int (*load_job)(job_info_msg_t **, uint32_t, uint16_t);
    void (*free_job_info_msg)(job_info_msg_t *);
    job_info_msg_t *job_buf = NULL;

    load_job = slurm_load_job;
    free_job_info_msg = slurm_free_job_info_msg;

    if ((*load_job)(&job_buf, jobid, SHOW_ALL) != 0) {
        slurm_error("nersc_perf: failed to load job data");
        return ESPANK_ERROR;
    }
    *shared = job_buf->job_array->shared;

    (*free_job_info_msg)(job_buf);
    return ESPANK_SUCCESS;
}

struct perf_config *parse_perf_config(int argc, char **argv) {
    int idx = 0;
    struct perf_config *config = (struct perf_config *)
            _malloc(sizeof(struct perf_config));
    memset(config, 0, sizeof(struct perf_config));

    for (idx = 0; idx < argc; idx++) {
        char *copy = NULL;
        char *key = NULL;
        char *value = NULL;

        copy = _strdup(argv[idx]);
        key = copy;
        value = strchr(copy, '=');
        if (value == NULL) {
            free(copy);
            copy = NULL;
            continue;
        }
        *value++ = '\0';

        if (strcasecmp(key, "base_start") == 0) {
             config->base_start = _strdup(value);
        } else if (strcasecmp(key, "base_stop") == 0) {
             config->base_stop = _strdup(value);
        } else if (strcasecmp(key, "modules") == 0) {
             char **modules = NULL;
             ssize_t modules_count = 0;
             ssize_t mod_idx = 0;
             modules_count = parse_csv_list(value, &modules);

             for (mod_idx = 0; mod_idx < modules_count; mod_idx++) {
                 struct perf_module_config *modconf =
                        (struct perf_module_config *)
                        _malloc(sizeof(struct perf_module_config));
                 memset(modconf, 0, sizeof(struct perf_module_config));
                 /* transfer control of the string to modconf */
                 modconf->module_name = modules[mod_idx];
                 modules[mod_idx] = NULL;

                 size_t alloc = sizeof(struct perf_module_config *)
                        * (config->n_modules + 1);
                 config->modules = (struct perf_module_config **)
                        _realloc(config->modules, alloc);
                 config->modules[config->n_modules] = modconf;
                 config->n_modules++;
             }
             if (modules != NULL) free(modules);
        } else {
             parse_module(config, key, value);
        }
        free(copy);
    }
    return config;
}

int check_auth(spank_t sp) {
    spank_context_t context = spank_context();

    /* validate user is authorized for these */
    int authorized = 0;
    uid_t uid = 0;
    gid_t gid = 0;
    gid_t *aux_gids = NULL;
    int n_gids = 0;
    int rc = ESPANK_SUCCESS;

    if (context == S_CTX_LOCAL || context == S_CTX_ALLOCATOR) {
        /* this process is running with user permissions */
        uid = getuid();
        gid = getgid();
        n_gids = getgroups(0, NULL);
        if (n_gids <= 0) return 0;
        aux_gids = (gid_t *) _malloc(sizeof(gid_t) * n_gids);
        int w_gids = getgroups(n_gids, aux_gids);
        if (w_gids != n_gids) {
            return 0;
        }
    } else {
        rc = spank_get_item(sp, S_JOB_UID, &uid);
        if (rc != ESPANK_SUCCESS) {
            slurm_error("nersc_perf: failed to get job uid, %d", rc);
            rc = ESPANK_ERROR;
        }
        if (spank_get_item(sp, S_JOB_GID, &gid) != ESPANK_SUCCESS) {
            slurm_error("nersc_perf: failed to get job gid");
            rc = ESPANK_ERROR;
        }
        if (spank_get_item(sp, S_JOB_SUPPLEMENTARY_GIDS, &aux_gids, &n_gids) != ESPANK_SUCCESS) {
            slurm_error("nersc_perf: failed to get job aux gids");
            rc = ESPANK_ERROR;
        }
    }
    if (rc == ESPANK_SUCCESS) {
        authorized = 0;
        int idx = 0;
        int uidx = 0;
        int gidx = 0;
        int gidx2 = 0;
        for (idx = 0; idx < n_user_requested_modules; idx++) {
            struct perf_module *perf = user_requested_modules + idx;
            if (!perf || !perf->version) {
                authorized++;
                continue;
            }
            if (perf->version->n_uids == 0 && perf->version->n_gids == 0) {
                authorized++;
                continue;
            }
            int auth = 0;
            for (uidx = 0; uidx < perf->version->n_uids; uidx++) {
                if (uid == perf->version->uids[uidx]) {
                    auth = 1;
                    break;
                }
            }
            for (gidx = 0; gidx < perf->version->n_gids; gidx++) {
                if (gid == perf->version->gids[gidx]) {
                    auth = 1;
                    break;
                }
                for (gidx2 = 0; gidx2 < n_gids; gidx2++) {
                    if (aux_gids[gidx2] == perf->version->gids[gidx]) {
                        auth = 1;
                        break;
                    }
                }
                if (auth) break;
            }
            if (auth) authorized++;
        }
    }
    return authorized;
}

int _perf_option_handler(int val, const char *optarg, int remote) {
    /* check if optarg is valid, if not assume old and go with current
     * default */
    ran_perf_option_handler = 1;
    if (config == NULL) return ESPANK_ERROR;
    if (optarg == NULL) return ESPANK_ERROR;

    char *_optarg = _strdup(optarg);
    char **selections = NULL;
    int rc = ESPANK_SUCCESS;
    ssize_t sel_count = 0;
    ssize_t sel_idx = 0;
    sel_count = parse_csv_list(_optarg, &selections);
    for (sel_idx = 0; sel_idx < sel_count; sel_idx++) {
        char *end = selections[sel_idx] + strlen(selections[sel_idx]);
        char *ptr = strchr(selections[sel_idx], '/');
        if (ptr == NULL) {
            ptr = end;
        }
        *ptr = '\0';
        ptr++;

        char *module_name = selections[sel_idx];
        char *version_str = NULL;
        if (ptr < end) {
            version_str = ptr;
        }
        struct perf_module_config *modconf = NULL;
        struct perf_module_instance *version = NULL;

        modconf = lookup_module(config, module_name);
        if (modconf == NULL) {
            slurm_error("nersc_perf: invalid perf module \"%s\"", module_name);
            rc = ESPANK_ERROR;
            goto terminate;
        }
        version = lookup_module_version(modconf, version_str);
        if (version == NULL && version_str == NULL) {
            version = lookup_module_version(modconf, modconf->default_version);
        }
        if (version == NULL) {
            slurm_error("nersc_perf: invalid perf module version specififed \"%s/%s\"", module_name, version_str);
            rc = ESPANK_ERROR;
            goto terminate;
        }
        int idx = 0;
        int found = 0;
        for (idx = 0; idx < n_user_requested_modules; idx++) {
            struct perf_module *test_module = &(user_requested_modules[idx]);
            if (modconf == test_module->module) {
                test_module->version = version;
                found = 1;
                break;
            }
        }

        if (!found) {
            user_requested_modules = _realloc(user_requested_modules, sizeof(struct perf_module) * (n_user_requested_modules + 1));
            user_requested_modules[n_user_requested_modules].module = modconf;
            user_requested_modules[n_user_requested_modules].version = version;
            n_user_requested_modules++;
        }
    }
terminate:
    if (_optarg) {
        free(_optarg);
        _optarg = NULL;
    }
    return rc;
}

static void _setup_cmd_option(spank_t sp) {
    int i = 0;
    int j = 0;
    char *usage = NULL;
    size_t usage_sz = 0;
    size_t usage_len = 0;

    if (config == NULL) return;
    if (perf_option.name != NULL) return;

    /* basic setup */
    perf_option.name = _strdup("perf");
    perf_option.arginfo = _strdup("<perf_module[/version][,more ...]>");
    perf_option.has_arg = 1;
    perf_option.val = 0;
    perf_option.cb = _perf_option_handler;

    /* setup usage */
    usage = NULL;
    usage = alloc_strcatf(usage, &usage_sz, &usage_len, "NERSC perf plugin. "
        "Possible values: ");

    for (i = 0; i < config->n_modules; i++) {
        struct perf_module_config *modconf = config->modules[i];
        usage = alloc_strcatf(usage, &usage_sz, &usage_len, "%s, ",
            modconf->module_name, modconf->module_name);
        for (j = 0; j < modconf->n_instances; j++) {
            int is_default = strcmp(modconf->instances[j]->version, modconf->default_version) == 0;
            usage = alloc_strcatf(usage, &usage_sz, &usage_len, "%s/%s%s, ",
                modconf->module_name, modconf->instances[j]->version,
                is_default ? "*" : "");
        }
    }
    usage = alloc_strcatf(usage, &usage_sz, &usage_len, " \"*\" indicates default");
    perf_option.usage = usage;
    spank_option_register(sp, &perf_option);
}

int slurm_spank_init(spank_t sp, int argc, char **argv) {
    int rc = ESPANK_SUCCESS;
    spank_context_t context = spank_context();

    config = parse_perf_config(argc, argv);

    _setup_cmd_option(sp);

    if (context == S_CTX_REMOTE) {
        uint32_t job_id = 0;
        if (spank_get_item(sp, S_JOB_ID, &job_id) != ESPANK_SUCCESS) {
            slurm_error("nersc_perf: failed to get job id");
            return ESPANK_ERROR;
        }

        /* prologs should have run, so status should exist */
        _read_perf_status_file(&status, job_id, &error_status);

        /* status has been read in slurmstepd now, in static, global variable
         * this will allow the slurm_spank_task_init() call to report any
         * errors to the user */
    }

    return rc;
}

int slurm_spank_init_post_opt(spank_t sp, int argc, char **argv) {
    int rc = ESPANK_SUCCESS;
    spank_context_t context = spank_context();
    int i = 0;
    int j = 0;
    int k = 0;

    if (context == S_CTX_LOCAL && n_user_requested_modules == 0) {
        /* check environment for any possible hits, if no option was applied */
        const char *optarg = getenv("_SLURM_SPANK_OPTION_nerscperf_perf");
        if (optarg != NULL) {
            rc = _perf_option_handler(0, optarg, 0);
            if (rc != ESPANK_SUCCESS) {
                goto terminate;
            }
        }
    }

    /* if the perf_option_handler has run, and yet there are no valid
     * modules selected, this is an error that we should prevent from
     * propagating */
    if (context == S_CTX_ALLOCATOR || context == S_CTX_LOCAL) {
        if (ran_perf_option_handler && n_user_requested_modules == 0) {
            slurm_error("No valid perf option specified.");
            exit(1);
        }
    }

    /* ensure there are no conflicts */
    if (context == S_CTX_ALLOCATOR || context == S_CTX_LOCAL) {
        for (i = 0; i < n_user_requested_modules; i++) {
            struct perf_module *q_module = user_requested_modules + i;
            for (j = 0; j < n_user_requested_modules; j++) {
                struct perf_module *t_module = user_requested_modules + j;
                if (i == j || t_module->version == NULL) continue;
                for (k = 0; k < t_module->version->n_incompatible; k++) {
                    struct perf_module *incompat = t_module->version->incompatible[k];
                    if (incompat->module == q_module->module &&
                        (incompat->version == NULL || incompat->version == q_module->version))
                    {
                        slurm_error("NERSC perf: cannot request both %s/%s and %s%s%s",
                            t_module->module->module_name, t_module->version->version,
                            incompat->module->module_name, (incompat->version != NULL ? "/" : ""),
                            (incompat->version != NULL ? incompat->version->version : ""));
                        rc = ESPANK_ERROR;
                    }
                }
            }
        }
        if (rc == ESPANK_ERROR) {
            exit(1);
        }
    }

    /* check if any required modules are missing */
    if (context == S_CTX_ALLOCATOR || context == S_CTX_LOCAL) {
        const char *_loaded_modules = getenv("LOADEDMODULES");
        char *loaded_modules = NULL;
        char *ptr = NULL;
        char *sptr = NULL;
        char *eptr = NULL;

        int idx = 0;
        size_t needed_modules = 0;
        size_t found_modules = 0;
        for (idx = 0; idx < n_user_requested_modules; idx++) {
            struct perf_module *q_module = user_requested_modules + idx;
            if (q_module && q_module->version && q_module->version->module) {
                if (idx < sizeof(needed_modules) * 8) {
                    needed_modules |= 1 << idx;
                }
            }
        }

        if (needed_modules > 0 && _loaded_modules != NULL) {
            loaded_modules = _strdup(_loaded_modules);
        }
        if (loaded_modules != NULL) {
            ptr = loaded_modules;
            sptr = loaded_modules;
            eptr = loaded_modules + strlen(loaded_modules);
            while (sptr < eptr) {
                ptr = strchr(sptr, ':');
                if (ptr == NULL) ptr = eptr;
                *ptr = '\0';

                char *loaded_module = sptr;
                int idx = 0;
                for (idx = 0; idx < n_user_requested_modules; idx++) {
                    struct perf_module *q_module = &(user_requested_modules[idx]);
                    if ((idx < sizeof(found_modules) * 8) &&
                        (needed_modules & (1 << idx)) &&
                        !(found_modules & (1 << idx)) &&
                        (strcmp(q_module->version->module, loaded_module) == 0))
                    {
                        found_modules |= (1 << idx);
                    }
                }

                sptr = ptr + 1;
            }
            free(loaded_modules);
            loaded_modules = NULL;
        }
        if (needed_modules != found_modules) {
            int idx = 0;
            for (idx = 0; idx < n_user_requested_modules && idx < sizeof(needed_modules) * 8; idx++) {
                if ((needed_modules & (1 << idx)) && !(found_modules & (1 << idx))) {
                    struct perf_module *q_module = &(user_requested_modules[idx]);
                    slurm_error("nersc_perf: missing required environmental module: %s", q_module->version->module);
                }
            }
            exit(1);
        }
    }

    int n_authorized = check_auth(sp);

    if (n_authorized != n_user_requested_modules) {
        slurm_error("nersc_perf: not authorized for all requested perf modules");
        exit(1);
    }

    if (n_user_requested_modules > 0) {
        char *ptr = NULL;
        size_t sz = 0;
        size_t len = 0;
        int idx = 0;
        for (idx = 0; idx < n_user_requested_modules; idx++) {
            ptr = alloc_strcatf(ptr, &sz, &len, "%s/%s,", user_requested_modules[idx].module->module_name, user_requested_modules[idx].version->version);
        }
        if (ptr != NULL) {
            size_t len = strlen(ptr);
            if (len > 0) {
                ptr[len - 1] = '\0';
            }
        }
        effective_perf_option_optarg = ptr;
        spank_setenv(sp, "_SLURM_SPANK_OPTION_nerscperf_perf", effective_perf_option_optarg, 1);
    }
terminate:
    return rc;
}

int slurm_spank_task_init(spank_t sp, int argc, char **argv) {
    /* config and status should be populated here! */
    int rc = ESPANK_SUCCESS;
    int idx = 0;

    if (n_user_requested_modules == 0) return rc;
    if (error_status != PERF_SUCCESS) {
        slurm_error("nersc_perf: %s", perf_status_str[error_status]);
    }
    for (idx = 0; idx < status.prologs.n_scripts; idx++) {
        if (status.prologs.exit_status[idx] != 0) {
            slurm_error("nersc_perf: failed to setup %s properly", status.prologs.modules[idx]);
            rc = ESPANK_ERROR;
        }
    }
    return rc;
}

int validate_script(const char *script, const char *module_name, const char *script_type) {
    int rc = ESPANK_SUCCESS;
    struct stat statData;
    if (script == NULL || module_name == NULL || script_type == NULL) {
        return ESPANK_ERROR;
    }
    if (stat(script, &statData) == 0) {
        if (statData.st_uid != 0) {
            slurm_error("nersc_perf: validate_script: %s %s script %s not "
                    "owned by uid 0", module_name, script_type, script);
            rc = ESPANK_ERROR;
            goto _validate_script_exit;
        }
        if (!(statData.st_mode & S_IXUSR)) {
            slurm_error("nersc_perf: validate_script: %s %s script %s is not "
                    "executable", module_name, script_type, script);
            rc = ESPANK_ERROR;
            goto _validate_script_exit;
        }
    } else {
        slurm_error("nersc_perf: validate_script: cannot find %s %s script %s",
                module_name, script_type, script);
        rc = ESPANK_ERROR;
        goto _validate_script_exit;
    }
_validate_script_exit:
    return rc;
}

/* stolen from shifter */
int forkAndExecvLogToSlurm(const char *appname, char **args) {
    int rc = ESPANK_SUCCESS;
    pid_t pid = 0;

    /* pipes for reading from setupRoot */
    int stdoutPipe[2];
    int stderrPipe[2];

    if (pipe(stdoutPipe) != 0) {
        slurm_error("FAILED to open stdout pipe! %s", strerror(errno));
        rc = ESPANK_ERROR;
        goto endf;
    }
    if (pipe(stderrPipe) != 0) {
        slurm_error("FAILED to open stderr pipe! %s", strerror(errno));
        rc = ESPANK_ERROR;
        goto endf;
    }
    pid = fork();
    if (pid < 0) {
        slurm_error("FAILED to fork %s", appname);
        rc = ESPANK_ERROR;
        goto endf;
    } else if (pid > 0) {
        int status = 0;
        FILE *stdoutStream = NULL;
        FILE *stderrStream = NULL;
        char *lineBuffer = NULL;
        size_t lineBuffer_sz = 0;


        /* close the write end of both pipes */
        close(stdoutPipe[1]);
        close(stderrPipe[1]);

        stdoutStream = fdopen(stdoutPipe[0], "r");
        stderrStream = fdopen(stderrPipe[0], "r");

        for ( ; stdoutStream && stderrStream ; ) {
            if (stdoutStream) {
                ssize_t nBytes = getline(&lineBuffer, &lineBuffer_sz, stdoutStream);
                if (nBytes > 0) {
                    slurm_info("%s stdout: %s", appname, lineBuffer);
                } else {
                    fclose(stdoutStream);
                    stdoutStream = NULL;
                }
            }
            if (stderrStream) {
                ssize_t nBytes = getline(&lineBuffer, &lineBuffer_sz, stderrStream);
                if (nBytes > 0) {
                    slurm_error("%s stderr: %s", appname, lineBuffer);
                } else {
                    fclose(stderrStream);
                    stderrStream = NULL;
                }
            }
        }

        /* wait on the child */
        slurm_info("waiting on %s", appname);
        waitpid(pid, &status, 0);
        if (WIFEXITED(status)) {
             rc = WEXITSTATUS(status);
        } else {
             rc = 1;
        }
        if (status != 0) {
            slurm_error("FAILED to run %s", appname);
            rc = ESPANK_ERROR;
            goto endf;
        }
    } else {
        /* close the read end of both pipes */
        close(stdoutPipe[0]);
        close(stderrPipe[0]);

        /* make the pipe stdout/err */
        dup2(stdoutPipe[1], STDOUT_FILENO);
        dup2(stderrPipe[1], STDERR_FILENO);
        close(stdoutPipe[1]);
        close(stderrPipe[1]);


        execv(args[0], args);
        exit(127);
    }
endf:
    return rc;
}

int _write_perf_status_file(struct perf_status *status, uint32_t job_id, int error_status) {
    FILE *fp = NULL;
    char filename[1024];
    char *cred = NULL;
    char *msg = NULL;
    size_t msg_sz = 0;
    size_t msg_len = 0;
    int idx = 0;
    int ret = PERF_SUCCESS;
    munge_ctx_t munge_ctx = munge_ctx_create();
    msg = alloc_strcatf(msg, &msg_len, &msg_sz, "error_status=%d\n", error_status);
    msg = alloc_strcatf(msg, &msg_len, &msg_sz, "n_prolog=%d\n", status->prologs.n_scripts);
    for (idx = 0; idx < status->prologs.n_scripts; idx++) {
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "prolog_script_%d=%s\n", idx, status->prologs.script_paths[idx]);
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "prolog_module_%d=%s\n", idx, status->prologs.modules[idx]);
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "prolog_exit_%d=%d\n", idx, status->prologs.exit_status[idx]);
    }
    msg = alloc_strcatf(msg, &msg_len, &msg_sz, "n_epilog=%d\n", status->epilogs.n_scripts);
    for (idx = 0; idx < status->epilogs.n_scripts; idx++) {
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "epilog_script_%d=%s\n", idx, status->epilogs.script_paths[idx]);
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "epilog_module_%d=%s\n", idx, status->epilogs.modules[idx]);
        msg = alloc_strcatf(msg, &msg_len, &msg_sz, "epilog_exit_%d=%d\n", idx, status->epilogs.exit_status[idx]);
    }
    if (munge_encode(&cred, munge_ctx, msg, strlen(msg)) != EMUNGE_SUCCESS) {
        ret = PERF_ERROR;
        goto terminate;
    }
    snprintf(filename, 1024, "/var/spool/slurmd/nersc_perf_job%u", job_id);
    fp = fopen(filename, "w");
    if (fp == NULL) {
        return PERF_ERROR;
    }
    fprintf(fp, "%s", cred);
    fclose(fp);
    fp = NULL;
terminate:
    if (fp != NULL) {
        fclose(fp);
        fp = NULL;
    }
    if (msg != NULL) {
        free(msg);
        msg = NULL;
    }
    if (cred != NULL) {
        free(cred);
        cred = NULL;
    }
    munge_ctx_destroy(munge_ctx);
    return ret;
}

size_t _read_data_from_file(const char *filename, char **data, size_t *datasz) {
    FILE *fp = NULL;
    const int bufsz = 2048;
    char buffer[bufsz];
    char *sptr = NULL;
    char *ptr = NULL;
    size_t sz = 0;
    size_t total_bytes = 0;

    fp = fopen(filename, "r");
    if (fp == NULL) {
        return 0;
    }
    while (!feof(fp) && !ferror(fp)) {
        size_t nbytes = fread(buffer, sizeof(char), bufsz, fp);
        while ((ptr - sptr) + nbytes >= sz) {
            size_t alloc_sz = sz + 4096;
            char *tmp = (char *) _realloc(sptr, alloc_sz * sizeof(char));
            ptr = tmp + (ptr - sptr);
            sptr = tmp;
            sz = alloc_sz;
        }
        memcpy(ptr, buffer, nbytes);
        ptr += nbytes;
        *ptr = '\0';

        total_bytes += nbytes;
    }
    fclose(fp);
    fp = NULL;

    *data = sptr;
    *datasz = sz;
    return total_bytes;
}

int _read_perf_status_file(struct perf_status *status, uint32_t job_id, int *error_status) {
    char filename[1024];
    int idx = 0;
    int rc = PERF_SUCCESS;
    char *cred = NULL;
    size_t cred_sz = 0;

    char *data = NULL;
    int data_len = 0;
    uid_t uid = 0;
    gid_t gid = 0;

    char *sptr = NULL;
    char *ptr = NULL;
    char *eptr = NULL;
    char *lineptr = NULL;

    munge_ctx_t munge_ctx = munge_ctx_create();
    munge_err_t munge_err = EMUNGE_SUCCESS;

    snprintf(filename, 1024, "/var/spool/slurmd/nersc_perf_job%u", job_id);
    if (_read_data_from_file(filename, &cred, &cred_sz) == 0) {
        slurm_error("nersc_perf: failed to read perf status file: %s", filename);
        rc = PERF_ERROR;
        goto terminate;
    }
    munge_err = munge_decode(cred, munge_ctx, (void **) &data, &data_len, &uid, &gid);
    if (munge_err != EMUNGE_SUCCESS &&
            munge_err != EMUNGE_CRED_EXPIRED &&
            munge_err != EMUNGE_CRED_REPLAYED)
    {
        slurm_error("nersc_perf: failed to decode perf status file: %s",
                munge_strerror(munge_err));
        rc = PERF_ERROR;
        goto terminate;
    }
    if (uid != 0 || gid != 0) {
        slurm_error("nersc_perf: credential not originated from root: %d:%d",
                uid, gid);
        rc = PERF_ERROR;
        goto terminate;
    }

    sptr = data;
    eptr = data + data_len;
    for (lineptr = sptr; lineptr < eptr; ) {
        sptr = lineptr;
        lineptr = strchr(sptr, '\n');
        if (lineptr == NULL) {
            lineptr = eptr;
        }
        *lineptr = '\0';
        lineptr++;
        ptr = strchr(sptr, '=');
        if (ptr == NULL) {
            slurm_error("nersc_perf: _read_perf_status: invalid read: %s", lineptr);
            continue;
        }
        *ptr = '\0';
        ptr++;
        if (strcmp(sptr, "error_status") == 0) {
            *error_status = atoi(ptr);
        } else if (strcmp(sptr, "n_prolog") == 0) {
            int n_prologs = atoi(ptr);
            allocate_script_set(&(status->prologs), n_prologs);
        } else if (strcmp(sptr, "n_epilog") == 0) {
            int n_epilogs = atoi(ptr);
            allocate_script_set(&(status->epilogs), n_epilogs);
        } else if (strncmp(sptr, "prolog_script_", 14) == 0) {
            idx = atoi(sptr + 14);
            if (idx < status->prologs.script_sz) {
                status->prologs.script_paths[idx] = _strdup(ptr);
            }
        } else if (strncmp(sptr, "epilog_script_", 14) == 0) {
            idx = atoi(sptr + 14);
            if (idx < status->epilogs.script_sz) {
                status->epilogs.script_paths[idx] = _strdup(ptr);
            }
        } else if (strncmp(sptr, "prolog_module_", 14) == 0) {
            idx = atoi(sptr + 14);
            if (idx < status->prologs.script_sz) {
                status->prologs.modules[idx] = _strdup(ptr);
            }
        } else if (strncmp(sptr, "epilog_module_", 14) == 0) {
            idx = atoi(sptr + 14);
            if (idx < status->epilogs.script_sz) {
                status->epilogs.modules[idx] = _strdup(ptr);
            }
        } else if (strncmp(sptr, "prolog_exit_", 12) == 0) {
            idx = atoi(sptr + 12);
            if (idx < status->prologs.script_sz) {
                status->prologs.exit_status[idx] = atoi(ptr);
            }
        } else if (strncmp(sptr, "epilog_exit_", 12) == 0) {
            idx = atoi(sptr + 12);
            if (idx < status->epilogs.script_sz) {
                status->epilogs.exit_status[idx] = atoi(ptr);
            }
        }
    }
terminate:
    munge_ctx_destroy(munge_ctx);
    if (cred != NULL) {
        free(cred);
        cred = NULL;
    }
    if (data != NULL) {
        free(data);
        data = NULL;
    }
    return rc;
}

int _unlink_perf_status_file(uint32_t job_id) {
    char filename[1024];
    snprintf(filename, 1024, "/var/spool/slurmd/nersc_perf_job%u", job_id);
    return unlink(filename);
}

int slurm_spank_job_prolog(spank_t sp, int argc, char **argv) {
    int rc = ESPANK_SUCCESS;
    uint32_t job_id = 0;
    uint16_t shared = 0;
    int idx = 0;

    slurm_debug("nersc_perf: starting prolog");

    if (spank_get_item(sp, S_JOB_ID, &job_id) != ESPANK_SUCCESS) {
        slurm_error("nersc_perf: failed to get job id");
        return ESPANK_ERROR;
    }

    if (config == NULL) {
        config = parse_perf_config(argc, argv);
    }
    if (config == NULL) {
        slurm_error("nersc_perf: failed to read config");
        return ESPANK_ERROR;
    }
    _setup_cmd_option(sp);

    const char *optarg = getenv("SPANK__SLURM_SPANK_OPTION_nerscperf_perf");
    if (optarg) {
        rc = _perf_option_handler(0, optarg, 0);
        if (rc != ESPANK_SUCCESS) {
            return ESPANK_ERROR;
        }
    }

    if (n_user_requested_modules == 0) {
        /* no perf modules requested! */
        return ESPANK_SUCCESS;
    }

    if (get_job_attributes(job_id, &shared) != ESPANK_SUCCESS) {
        slurm_error("nersc_perf: failed to read job attributes");
        return ESPANK_ERROR;
    }

    if (shared != 0) {
        slurm_error("nersc_perf: cannot setup perf-collection environment in shared job");
        error_status = PERF_NO_SHARED;
        _write_perf_status_file(&status, job_id, error_status);
        return ESPANK_SUCCESS;
    }

    /* plan out all the prologs (and epilogs) that will need to be run */
    if (config->base_start) {
        error_status = add_prolog(sp, &status, config->base_start, "perf_base");
        if (error_status != PERF_SUCCESS) {
            _write_perf_status_file(&status, job_id, error_status);
            return ESPANK_SUCCESS;
        }
        slurm_debug("nersc_perf: added prolog %s to list", config->base_start);
    }
    if (config->base_stop) {
        error_status = add_epilog(sp, &status, config->base_stop, "perf_base");
        if (error_status != PERF_SUCCESS) {
            _write_perf_status_file(&status, job_id, error_status);
            return ESPANK_SUCCESS;
        }
        slurm_debug("nersc_perf: added epilog %s to list", config->base_stop);
    }
    for (idx = 0; idx < n_user_requested_modules; idx++) {
        struct perf_module *pmod = user_requested_modules + idx;
        if (pmod == NULL || pmod->version == NULL) continue;
        if (pmod->version->start) {
            error_status = add_prolog(sp, &status, pmod->version->start,
                    pmod->module->module_name);
            if (error_status != PERF_SUCCESS) {
                _write_perf_status_file(&status, job_id, error_status);
                return ESPANK_SUCCESS;
            }
        }
        if (pmod->version->stop) {
            error_status = add_epilog(sp, &status, pmod->version->stop,
                    pmod->module->module_name);
            if (error_status != PERF_SUCCESS) {
                _write_perf_status_file(&status, job_id, error_status);
                return ESPANK_SUCCESS;
            }
        }
    }

    slurm_debug("nersc_perf: about to run %d prologs", status.prologs.n_scripts);

    /* run each prolog */
    for (idx = 0; idx < status.prologs.n_scripts; idx++) {
        char *args[] = {
            status.prologs.script_paths[idx],
            NULL
        };
        slurm_info("nersc_perf: about to run prolog for %s: %s",
                status.prologs.modules[idx], status.prologs.script_paths[idx]);
        int rc = forkAndExecvLogToSlurm(args[0], args);
        status.prologs.exit_status[idx] = rc;
        slurm_info("nersc_perf: completed prolog for %s: %s (exit: %d)",
                status.prologs.modules[idx], status.prologs.script_paths[idx],
                rc);
    }
    _write_perf_status_file(&status, job_id, PERF_SUCCESS);
    rc = ESPANK_SUCCESS;
    return rc;
}

int slurm_spank_job_epilog(spank_t sp, int argc, char **argv) {
    int rc = ESPANK_SUCCESS;
    uint32_t job_id = 0;
    int idx = 0;

    slurm_debug("nersc_perf: starting epilog");

    if (spank_get_item(sp, S_JOB_ID, &job_id) != ESPANK_SUCCESS) {
        slurm_error("nersc_perf: failed to get job id");
        return ESPANK_ERROR;
    }

    if (config == NULL) {
        config = parse_perf_config(argc, argv);
    }
    if (config == NULL) {
        slurm_error("nersc_perf: failed to read config");
        return ESPANK_ERROR;
    }
    _setup_cmd_option(sp);

    const char *optarg = getenv("SPANK__SLURM_SPANK_OPTION_nerscperf_perf");
    if (optarg) {
        rc = _perf_option_handler(0, optarg, 0);
        if (rc != ESPANK_SUCCESS) {
            return ESPANK_ERROR;
        }
    }

    if (n_user_requested_modules == 0) {
        /* no perf modules requested! */
        return ESPANK_SUCCESS;
    }
    slurm_debug("nersc_perf: epilog have %d n_user_requested_modules", n_user_requested_modules);

    if (_read_perf_status_file(&status, job_id, &error_status) != PERF_SUCCESS) {
        slurm_error("nersc_perf: failed to read status file for job id: %d", job_id);
        return -1;
    }
    slurm_debug("nersc_perf: epilog found %d epilogs to run", status.epilogs.n_scripts);

    for (idx = 0; idx < status.epilogs.n_scripts; idx++) {
        char *args[] = {
            status.epilogs.script_paths[idx],
            NULL
        };
        slurm_info("nersc_perf: about to run epilog for %s: %s",
                status.epilogs.modules[idx], status.epilogs.script_paths[idx]);
        int exitstatus = forkAndExecvLogToSlurm(args[0], args);
        status.epilogs.exit_status[idx] = exitstatus;
        slurm_info("nersc_perf: completed epilog for %s: %s (exit: %d)",
                status.epilogs.modules[idx], status.epilogs.script_paths[idx],
                exitstatus);
        if (exitstatus != 0) {
            rc = -1;
        }
    }
    return rc;
}
