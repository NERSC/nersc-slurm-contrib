#!/bin/bash
rc=0

## disable uncore examination
echo 1 > /proc/sys/kernel/perf_event_paranoid || rc=1

## re-secure kernel pointers
echo 1 > /proc/sys/kernel/kptr_restrict || rc=1

## resume ldms syspapi sampling
echo -n resume | /opt/ovis/sbin/ldmsd_stream_publish -a munge -x sock -h localhost -p 410 -s syspapi_stream -t string || rc=1

exit $rc
