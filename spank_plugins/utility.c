/*
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

/**
 * alloc_strcatf appends to an existing string (or creates a new one) based on
 * user-supplied format in printf style.  The current capacity and length of
 * the string are used to determine where to write in the string and when to
 * allocate.  An eager-allocation strategy is used where the string capacity is
 * doubled at each reallocation.  This is done to improve performance of
 * repeated calls
 * Parameters:
 *      string - pointer to string to be extended, can be NULL
 *      currLen - pointer to integer representing current limit of used portion
 *                of string
 *      capacity - pointer to integer representing current capacity of string
 *      format - printf style formatting string
 *      ...    - variadic arguments for printf
 *
 * The dereferenced value of currLen is modified upon successful concatenation
 * of string.
 * The dereferenced value of capacity is modified upon successful reallocation
 * of string.
 *
 * Returns pointer to string, possibly realloc'd.  Caller should treat this
 * function like realloc (test output for NULL before assigning).
 * Returns NULL upon vsnprintf error or failure to realloc.
 */
char *alloc_strcatf(char *string, size_t *currLen, size_t *capacity, const char *format, ...) {
    int status = 0;
    int n = 0;

    if (currLen == 0 || capacity == 0 || format == NULL) {
        return NULL;
    }
    if (*currLen > *capacity) {
        return NULL;
    }

    if (string == NULL || *capacity == 0) {
        string = (char *) malloc(sizeof(char) * 128);
        *capacity = 128;
    }

    while (1) {
        char *wptr = string + *currLen;
        va_list ap;

        va_start(ap, format);
        n = vsnprintf(wptr, *capacity - (*currLen), format, ap);
        va_end(ap);

        if (n < 0) {
            /* error, break */
            status = 1;
            break;
        } else if ((size_t) n >= (*capacity - *currLen)) {
            /* if vsnprintf returns larger than allowed buffer, need more space
             * allocating eagerly to reduce cost for successive strcatf
             * operations */
            size_t newCapacity = *capacity * 2 + 1;
            char *tmp = NULL;
            if (newCapacity < (size_t) (n + 1)) {
                newCapacity = (size_t) (n + 1);
            }

            tmp = (char *) realloc(string, sizeof(char) * newCapacity);
            if (tmp == NULL) {
                status = 2;
                break;
            }
            string = tmp;
            *capacity = newCapacity;
        } else {
            /* success */
            status = 0;
            *currLen += n;
            break;
        }
    }
    if (status == 0) {
        return string;
    }
    return NULL;
}
