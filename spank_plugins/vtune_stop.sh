#!/bin/bash

## do not set -e, need to ensure we get to the bottom of the script

VTUNE_DRIVER_PATH=/lib/modules/$(uname -r)/extra/vtune
$VTUNE_DRIVER_PATH/pax/rmmod-pax
$VTUNE_DRIVER_PATH/vtsspp/rmmod-vtsspp
$VTUNE_DRIVER_PATH/socperf/src/rmmod-socperf
$VTUNE_DRIVER_PATH/rmmod-sep

## disable uncore examination
echo 1 > /proc/sys/kernel/perf_event_paranoid

## re-secure kernel pointers
echo 1 > /proc/sys/kernel/kptr_restrict
