#!/bin/bash
set -e
VTUNE_DRIVER_PATH=/lib/modules/$(uname -r)/extra/vtune
$VTUNE_DRIVER_PATH/pax/insmod-pax -p 666
$VTUNE_DRIVER_PATH/vtsspp/insmod-vtsspp -p 666
$VTUNE_DRIVER_PATH/socperf/src/insmod-socperf -p 666
$VTUNE_DRIVER_PATH/insmod-sep -p 666 -pu

## allow uncore examination
echo 0 > /proc/sys/kernel/perf_event_paranoid

## allow kernel pointers to be written unrestricted
echo 0 > /proc/sys/kernel/kptr_restrict
