slurm.log_info("loading job_submit.lua")

package.path = package.path .. ';/usr/lib/nersc-slurm-contrib/?.lua'
local jsl = require "lib_job_submit"

jsl.setupFromYaml("/etc/slurm/policy.yaml")


return slurm.SUCCESS
