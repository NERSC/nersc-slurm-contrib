--[[
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
]]--

local yaml = require 'yaml'
local socket = require 'socket'
local jsl = {}
local site = {}
local isTestHarness = _G['isTestHarness']

local slurm
if isTestHarness and not _G['slurm'] then
    slurm = require 'slurm.lua'
else
    slurm = _G['slurm']
end

local redis_host = '127.0.0.1'
local redis_port = 6379
local redis_conf = '/etc/redis/slurm-redis.conf'
local redis_password = nil

--- Simple iterator for producing values of a list.
--
-- anonIter generates a new table from the passed table allowing this
-- function to re-index the table so that all values can be produced.
-- Ordering is only guaranteed for basic lists (numerically indexed
-- basic tables that don't change the behavior of pairs() in some way.
--
-- @param obj table to be iterated over
-- @return iterator function for provided table
local function anonIter(obj)
    local keys = {}
    local values = {}
    for key,value in pairs(obj) do
        table.insert(keys, key)
        table.insert(values, value)
    end

    local idx = 0
    return function ()
        idx = idx + 1
        return values[idx]
    end
end

function read_redis_password(fname)
    local fp = assert(io.open(fname, 'r'))
    local input = fp:read()
    while input do
        if string.find(input, "requirepass") then
             local idx = string.find(input, ' ')
             fp:close()
             return string.sub(input, idx + 1)
        end
        input = fp:read()
    end
    fp:close()
    return nil
end
if not isTestHarness then
    redis_password = read_redis_password(redis_conf)
end

function redis_connect(host, port, password)
    local ip = assert(socket.dns.toip(host))
    local client = socket.tcp()
    client:settimeout(0.2)
    client:connect(ip, port)
    if client and password then
        client:send("AUTH " .. password .. "\n")
        local resp = client:receive()
        if resp ~= "+OK" then
            client:close()
            return nil
        end
    end
    return client
end

function redis_read_bulkstring(client)
    local input = client:receive()
    if not input or string.sub(input, 1, 1) ~= "$" then
        return nil
    end
    local nchar = tonumber(string.sub(input, 2))
    if nchar < 0 then
        return nil
    end
    input = client:receive()
    return input
end

function redis_get(client, query)
    client:send("GET \"" .. query .. "\"\r\n")
    return redis_read_bulkstring(client)
end

-- Class SlurmError
-- Simple class meant to encapsulate internal- and external-facing error
-- messages, help generate stack stack traces in the slurm logs, and
-- generally increase level of available debugging information for tracking
-- down problems in the job_submit/cli_filter lua code.
--
-- Intended to be thrown with error() at almost any point in the code to
-- result in rejection of the job and facilite user communication and
-- debugging.
local SlurmError = {}

--- Create new instance of SlurmError.
--
-- Allocates a new table, sets up the SlurmError metatable.
-- Special attributes are the __tostring to get easy conversion to a string
-- using the internal error message.
--
-- @param code intended integer return error code (i.e. slurm.ERROR).  Anything
--             other then slurm.SUCCESS will result in the job submission or
--             modification being rejected
-- @param userMsg string message to be displayed to user via slurm.log_user
-- @param slurmMsg string message to be put in log via slurm.log_error
-- @return generated instance of SlurmError
function SlurmError:new(code, userMsg, slurmMsg)
    local obj = {}
    obj.code = code
    obj.userMsg = userMsg
    obj.slurmMsg = slurmMsg
    obj.debug = false

    setmetatable(obj, self)
    self.__index = self
    self.__tostring = function (e) return 'ERROR: '.. e.slurmMsg end

    return obj
end

function SlurmError:enableDebugging()
    self.debug = true
end
-- END Class SlurmError --

-- Class DerivedJobData --
-- All job-dependent or job-level provided data and functions operating
-- directly on that data are gathered into this class.  Gives a consistent
-- method for determining state of a job as it is being processed.
local DerivedJobData = {}

--- Initialize internal job data class.
-- 
function DerivedJobData:new(job_request, job_record, part_list, operation)
    local obj = {}

    setmetatable(obj, self)
    self.__index = self

    obj.Request = job_request or {}
    obj.Record = job_record or {}
    obj.PartList = part_list or {}
    obj.ToWrite = {}
    obj.PendingArrayTasks = obj.PendingArrayTasks or 1
    obj.Operation = operation
    obj.FailureReason = {}

    if not site.isTestHarness then
        obj:_init_Username()
        obj:_init_Account()
        obj:_init_Balances()
        obj:_init_NodeClass()

        -- _init_FilesystemLicenses will set ToWrite.licenses
        -- all functions _writing_ to licenses should _read_ that
        -- version
        obj:_init_FilesystemLicenses()

        -- perform any spank processing
        obj:_init_Spank()
    end
    return obj
end

function DerivedJobData:_init_Username()
    self.Username = self.Request.user_name
    if not self.Username then
        local msg = "Failed to lookup username.  Please try again later, something appears to be wrong."
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
end

function DerivedJobData:_init_Account()
    local account = self.Record.account
    if self.Request.account then account = self.Request.account end
    if not account then account = self.Request.default_account end
    if not account then
        local msg = "Unable to determine account name.  Please resubmit your job specifying account with -A."
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    self.Account = account
end

function DerivedJobData:_init_Balances()
    if not self.Account then self:_init_Account() end
    if not self.Username then self:_init_Username() end
    if not self.Account or not self.Username then return end

    if not isTestHarness then
        local client = redis_connect(redis_host, redis_port, redis_password)
        if client then
            local query = 'account:' .. self.Account .. ':mpp:'
            self.AccountBalance = tonumber(redis_get(client, query))
            query = 'remaining:' .. self.Username .. ':' .. self.Account .. ':mpp:'
            self.UserBalance = tonumber(redis_get(client, query))
            client:close()
        end
    else
        -- use legacy method when using test harness
        local command = string.format("%s/getUserBalance.py %s %s",
                                      site.nerscSlurmPluginDir, self.Username,
                                      self.Account)
        local fp = assert(io.popen(command))
        local balance_str = fp:read("*all")
        fp:close()

        if balance_str then
            local delimIdx = string.find(balance_str, ',')
            if delimIdx ~= nil then
                self.UserBalance = tonumber(string.sub(balance_str, 1, delimIdx - 1))
                self.AccountBalance = tonumber(string.sub(balance_str, delimIdx + 1))
            else
                self.UserBalance = tonumber(balance_str)
            end
        end
    end
end

function DerivedJobData:_init_NodeClass()
    local to_write
    self.NodeClass,self.NodeClassFeatures,to_write = self:_determine_node_class()

    if not self.NodeClass then
        slurm.log_info("Unable to determine hardware architecture, hopefully matched queue implies one")
    end
    if self.NodeClass and to_write then
        self.ToWrite.features = to_write
    end
end

function DerivedJobData:_init_ArrayTasks()
    local pending_tasks = nil
    if self.Request.array_inx then
        pending_tasks = self:_get_request_array_task_count()
    elseif self.Record.array_task_cnt then
        pending_tasks = self.Record.array_task_cnt
    end
    if not pending_tasks or pending_tasks < 1 then
        pending_tasks = 1
    end

    self.PendingArrayTasks = pending_tasks
end

function _escape(str)
    str = str:gsub("\\", "\\\\")
    str = str:gsub("\"", "\\\"")
    str = str:gsub("'", "\\'")
    str = str:gsub("\b", "\\b")
    str = str:gsub("\f", "\\f")
    str = str:gsub("\n", "\\n")
    str = str:gsub("\r", "\\r")
    str = str:gsub("\t", "\\t")
    str = str:gsub("<", "\\u003C")
    return str
end

function DerivedJobData:apply(queue)
    for key,value in pairs(self.ToWrite) do
        if key == "AdminCommentJson" then
            -- user updates cannot change admin_comment, so do not try
            if  self.Operation == "submit" then
                local output = "{"
                local idx = 0
                for k,v in pairs(value) do
                    local item = "\"" .. _escape(k) .. "\":\"" .. _escape(v) .. "\""
                    if idx > 0 then
                        output = output .. "," .. item
                    else
                        output = output .. item
                    end
                    idx = idx + 1
                end
                output = output .. "}"
                self.Request['admin_comment'] = output
            end
        else
            if self.Record[key] ~= value or (self.Request[key] and self.Request[key] ~= value) then
                slurm.log_info("apply(): setting %s to %s", key, tostring(value))
                self.Request[key] = value
            end
        end
    end

    -- write out queue execution statements
    for key,value in pairs(queue.Apply) do
        local functionName = "apply_" .. key
        if queue[functionName] then
            queue[functionName](queue, value, self)
        end
    end
end

function DerivedJobData:_init_FilesystemLicenses()
    local adj_licenses = {}
    local fsLicenseCount = 0
    local __pairsByKeys = function (t, f)
        local a = {}
        for n in pairs(t) do table.insert(a, n) end
        table.sort(a, f)
        local i = 0
        local iter = function ()
            i = i + 1
            if a[i] == nil then return nil
            else return a[i],t[a[i]]
            end
        end
        return iter
    end

    local src_licenses = nil
    for source in anonIter{self.ToWrite, self.Request, self.Record} do
        local licenses = source['licenses']
        if licenses ~= nil then
            src_licenses = licenses
            break
        end
    end
    if not src_licenses then
        src_licenses = ''
    end

    for license in string.gmatch(src_licenses, '([^,]+)') do
        local licenseStripped
        local licenseCnt
        licenseStripped,licenseCnt = string.match(license, "([^:]+):([^:]+)")
        if licenseStripped then
            license = licenseStripped
        end

        if license == "SCRATCH" and self.Request.environment then
            license = nil
            local userScratch = self.Request.environment.SCRATCH
            if userScratch then
                for fsLicense,patterns in pairs(site.filesystemPatterns) do
                    for pidx = 1, #patterns do
                        if string.match(userScratch, patterns[pidx]) then
                            license = fsLicense
                        end
                    end
                end
            end
        end
        if license then
            for idx = 1, #site.filesystemLicenses do
                if license == site.filesystemLicenses[idx] then
                    fsLicenseCount = fsLicenseCount + 1
                    licenseCnt = 1
                end
            end
            if not licenseCnt then
                licenseCnt = 1
            end
            adj_licenses[license] = licenseCnt
        end
    end

    -- automatically add a license for TMPDIR and working dir
    local autoadd = {}
    local tmpdir = nil
    if self.Request.environment and self.Request.environment.TMPDIR then
        tmpdir = self.Request.environment.TMPDIR
    end
    if tmpdir then
        table.insert(autoadd, tmpdir)
    end

    local workdir = self.Request.work_dir
    if not workdir then
        workdir = self.Record.work_dir
    end
    if workdir then
        table.insert(autoadd, workdir)
    end

    for aidx = 1,#autoadd do
        for fsLicense,patterns in pairs(site.filesystemPatterns) do
            for pidx = 1,#patterns do
                if string.match(autoadd[aidx], patterns[pidx]) ~= nil then
                     if adj_licenses[fsLicense] == nil then
                         fsLicenseCount = fsLicenseCount + 1
                         adj_licenses[fsLicense] = 1
                     end
                end
            end
        end
    end

    -- construct license string
    local licenseStr = ""
    for license,licenseCnt in __pairsByKeys(adj_licenses) do
        licenseStr = licenseStr .. "," .. license .. ":" .. licenseCnt
    end

    -- set the license string
    if string.len(licenseStr) > 0 then
        licenseStr = string.sub(licenseStr, 2)
        self.ToWrite.licenses = licenseStr
    end
    return licenseStr,fsLicenseCount
end

function DerivedJobData:_exec_action(key, value, auxvalue)
    local func = 'apply_' .. key
    if self[func] then
        self[func](self, value, auxvalue)
    else
        slurm.log_error('WARNING: ' .. key .. ' not implemented')
    end
end

function DerivedJobData:_init_Spank()
    local spank = {}
    local site_spank_env = site.spank.env or {}
    local site_spank_opt = site.spank.option or {}
    for source in anonIter{self.Request,self.Record} do
        local idx = 0
        local limit = source['spank_job_env_size'] or 0
        while idx < limit do
            local val = source['spank_job_env'][idx]
            local splitfxn = string.gmatch(val, '([^=]+)')
            local key = splitfxn()
            local value = splitfxn()
            spank[key] = value
            idx = idx + 1
        end
    end
    for env,actions in pairs(site_spank_env) do
        if spank[env] then
            for action in anonIter(actions) do
                for k,v in pairs(action) do
                    self:_exec_action(k, v, spank[env])
                end
            end
        end
    end
    for plugin,options in pairs(site_spank_opt) do
        for option,actions in pairs(options) do
            var = '_SLURM_SPANK_OPTION_' .. plugin .. '_' .. option
            if spank[var] then
                for action in anonIter(actions) do
                    for k,v in pairs(action) do
                        self:_exec_action(k, v, spank[var])
                    end
                end
            end
        end
    end
end

--- getNodes - used to determine the requested number of nodes from any source
--     this should node be used as a general purpose function, only to determine
--     if at some point the user specified -N to express themselves
--     use _get_effective_nodes() for a more general purpose value
function DerivedJobData:getNodes()
    for source in anonIter{self.ToWrite,self.Request,self.Record} do
        local lmin = source['min_nodes'] or slurm.NO_VAL
        local lmax = source['max_nodes'] or slurm.NO_VAL

        if lmin ~= slurm.NO_VAL and lmax == slurm.NO_VAL then
            lmax = lmin
        end
        if lmin ~= slurm.NO_VAL then
            if lmax < lmin then
                lmax = lmin
            end
            return lmin,lmax
        end
    end
    return nil,nil
end

function DerivedJobData:getCpus()
    for source in anonIter{self.ToWrite,self.Request,self.Record} do
        local lmin = source['min_cpus'] or slurm.NO_VAL
        local lmax = source['max_cpus'] or slurm.NO_VAL

        if lmin ~= slurm.NO_VAL and lmax == slurm.NO_VAL then
            lmax = lmin
        end
        if lmin ~= slurm.NO_VAL then
            if lmax < lmin then
                lmax = lmin
            end
            return lmin,lmax
        end
    end
    return nil,nil
end

function DerivedJobData:getNumTasks()
    for source in anonIter{self.ToWrite,self.Request} do
        local ntasks = source['num_tasks'] or slurm.NO_VAL
        if ntasks ~= slurm.NO_VAL then
            return ntasks
        end
    end
    return 1
end

function DerivedJobData:getCpusPerTask(arch)
    for source in anonIter{self.ToWrite, self.Request} do
        local value = source['cpus_per_task'] or slurm.NO_VAL16
        if value ~= slurm.NO_VAL16 then
            return value
        end
    end
    if arch and site.arch[arch].default_cpus_per_task then
        return site.arch[arch].default_cpus_per_task
    end
    return 1
end

function DerivedJobData:getNTasksPerCore()
    for source in anonIter{self.ToWrite, self.Request} do
        local value = source['ntasks_per_core'] or slurm.NO_VAL16
        if value ~= slurm.NO_VAL16 then
            return value
        end
    end
    return nil
end

function DerivedJobData:getNTasksPerSocket()
    for source in anonIter{self.ToWrite, self.Request} do
        local value = source['ntasks_per_socket'] or slurm.NO_VAL16
        if value ~= slurm.NO_VAL16 then
            return value
        end
    end
    return nil
end

function DerivedJobData:getNTasksPerNode()
    for source in anonIter{self.ToWrite, self.Request} do
        local value = source['ntasks_per_node'] or slurm.NO_VAL16
        if value ~= slurm.NO_VAL16 then
            return value
        end
    end
    return nil
end

function DerivedJobData:_calculate_core_resources(queue)
    local arch = self.NodeClass
    if not arch and site.default_arch then
        arch = site.default_arch
    end
    if queue.Apply.ExecutionArch and arch ~= queue.Apply.ExecutionArch then
        arch = queue.Apply.ExecutionArch
    end

    if not arch then
        local msg = "No architecture specified, cannot estimate job costs."
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    if not site.arch[arch] then
        local msg = "Invalid architecture " .. arch .. " specified."
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    if not site.arch[arch].enabled then
        local msg = "Architecture " .. arch .. " is not enabled."
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end

    local nodes = nil
    local cpus = nil
    local min_nodes,max_nodes = self:getNodes()
    local min_cpus,max_cpus = self:getCpus()
    local num_tasks = self:getNumTasks()

    local cores_per_socket = site.arch[arch].cores_per_socket
    local sockets_per_node = site.arch[arch].sockets_per_node
    local cpus_per_core = site.arch[arch].cpus_per_core

    local cpus_per_task = self:getCpusPerTask(arch)

    local ntasks_per_core = self:getNTasksPerCore()
    local ntasks_per_socket = self:getNTasksPerSocket()
    local ntasks_per_node = self:getNTasksPerNode()

    -- simplest when user just asks for -N
    if max_nodes then
        -- intentionally using max_nodes here, since above test is really only
        -- used to determine if user is making a request for the number of nodes
        -- required.  max_nodes is automatically disentangled with getNodes()
        nodes = max_nodes

    elseif num_tasks then
        -- allow user to use -n (MPI tasks)
        nodes = math.ceil(num_tasks / ((cores_per_socket * sockets_per_node * cpus_per_core) / cpus_per_task))
        cpus = num_tasks * cpus_per_task

        if ntasks_per_core then
            local ncores = num_tasks / ntasks_per_core
            nodes = math.ceil( ncores / (cores_per_socket * sockets_per_node ) )
        elseif ntasks_per_socket then
            local nsockets = num_tasks / ntasks_per_socket
            nodes = math.ceil( nsockets / sockets_per_node )
        elseif ntasks_per_node then
            local nnodes = num_tasks / ntasks_per_node
            nodes = nnodes
        end
    end

    if not num_tasks then
        num_tasks = 1
    end

    if queue:isNodeExclusive(self.PartList) then
        local nnodes
        if nodes then
            nnodes = nodes
        elseif max_nodes then
            nnodes = max_nodes
        else
            nnodes = 1
        end
        cpus = nnodes * sockets_per_node * cores_per_socket * cpus_per_core
    else
        -- queue allows job sharing, so allow partial node allocations
        -- other than explicit cpu requests, users can request --mem or --mem-per-cpu
        local min_mem_per_node = nil
        local cpus_from_mem = nil
        local part = queue:getExecutionPartition()
        for source in anonIter{self.ToWrite, self.Request, self.Record} do
            local value = source['min_mem_per_node']
            if value then
                min_mem_per_node = value
                break
            end
        end

        if min_mem_per_node then
            local def_mem_per_cpu = self.PartList[part].def_mem_per_cpu
            if not max_nodes then
                max_nodes = num_tasks -- default of one task per node
            end
            if def_mem_per_cpu then
                cpus_from_mem = math.ceil(min_mem_per_node / def_mem_per_cpu)
                cpus_from_mem = cpus_from_mem * max_nodes
slurm.log_debug("def_mem_per_cpu: %s", tostring(def_mem_per_cpu))
            end
slurm.log_debug("min_mem_per_node: %s", tostring(min_mem_per_node))
slurm.log_debug("cpus_from_mem: %s", tostring(cpus_from_mem))
        end

        --[[ none of this is needed, keeping in case it is later
        local min_mem_per_cpu = nil
        for source in anonIter{self.ToWrite, self.Request, self.Record} do
            local value = source['min_mem_per_cpu']
            if value then
                min_mem_per_node = value
                break
            end
        end

        if min_mem_per_cpu then
            local max_mem_per_cpu = self.PartList[part].max_mem_per_cpu
            if max_mem_per_cpu and min_mem_per_cpu > max_mem_per_cpu then
                min_mem_per_cpu = max_mem_per_cpu
            end
        end
        --]]

        if max_cpus and not cpus then
            cpus = max_cpus
        end

        if max_cpus and  cpus and max_cpus > cpus then
            cpus = max_cpus
        end

        if not cpus then
            cpus = cpus_per_task * num_tasks
        end

        if cpus_from_mem and cpus_from_mem > cpus then
            cpus = cpus_from_mem
        end
    end

    return math.ceil(cpus / cpus_per_core)
end

function DerivedJobData:_get_effective_nodes(queue)
    local req_core = self:_calculate_core_resources(queue)
    local arch = self.NodeClass
    if not arch and site.default_arch then
        arch = site.default_arch
    end
    if queue.Apply.ExecutionArch and arch ~= queue.Apply.ExecutionArch then
        arch = queue.Apply.ExecutionArch
    end

    local cores_per_socket = site.arch[arch].cores_per_socket
    local sockets_per_node = site.arch[arch].sockets_per_node

    return req_core / (cores_per_socket * sockets_per_node)
end

function DerivedJobData:getArchChargeFactor()
    local arch = self.NodeClass
    if not arch and site.default_arch then
        arch = site.default_arch
    end
    if not arch then return 1 end
    return site.arch[arch].chargeFactor or 1
end

function DerivedJobData:getTimeLimit(queue)
    local timeLimit = nil
    for source in anonIter{self.ToWrite, self.Request, self.Record} do
        local limit = source['time_limit']
        if limit and limit ~= slurm.NO_VAL then
            timeLimit = limit
            break
        end
    end
    if not timeLimit then
        local part = queue:getExecutionPartition()
        part = self.PartList[part] or {}
        if part.default_time and part.default_time ~= slurm.NO_VAL then
            timeLimit = part.default_time
        elseif part.max_time and part.max_time ~= slurm.NO_VAL then
            timeLimit = part.max_time
        end
    end
    return timeLimit
end

function DerivedJobData:_determine_node_class()
    local features = nil
    local node_class = nil
    local extra_features = nil
    local write_features = nil

    for source in anonIter{self.ToWrite, self.Request, self.Record} do
        local l_features = source['features']
        if l_features ~= nil then
            features = l_features
            break
        end
    end
    if not features then
        features = ''
    end

    if site.arch.knl and site.arch.knl.enabled and string.find(features, 'knl') then
        write_features,node_class,extra_features = self:_parse_knl_features(features)
    elseif site.arch[features] and site.arch[features].enabled then
        node_class = features
    end

    if not write_features and node_class then
        write_features = node_class
    end

    if not node_class and site.default_arch and site.arch[site.default_arch]
        and site.arch[site.default_arch].enabled then

        node_class = site.default_arch
        write_features = site.default_arch
    end

    return node_class,extra_features,write_features
end

--[[  parse_knl_features - special node features handling for the complex
      case of the knights landing cpu ]]--
function DerivedJobData:_parse_knl_features(features)
    local knl_numa = {'quad','snc2','snc4','hemi','a2a'}
    local knl_mcdram = {'flat','split','equal','cache'}

    local sel_knl_numa = nil
    local sel_knl_mcdram = nil

    local node_class = 'knl'
    for idx = 1, #knl_numa do
        if string.find(features, knl_numa[idx]) ~= nil then
            sel_knl_numa = knl_numa[idx]
        end
    end
    for idx = 1, #knl_mcdram do
        if string.find(features, knl_mcdram[idx]) ~= nil then
            sel_knl_mcdram = knl_mcdram[idx]
        end
    end
    if features == "knl" and not sel_knl_numa and not sel_knl_mcdram then
        sel_knl_numa = site.arch.knl.default_numa
        sel_knl_mcdram = site.arch.knl.default_mcdram
    end
    if not sel_knl_numa or not sel_knl_mcdram then
        slurm.log_user('knl constraint selected with unknown numa and/or mcdram modes')
        return nil,nil,nil
    end

    local write_features = string.format("knl&%s&%s", sel_knl_numa, sel_knl_mcdram)
    local special = string.format("%s&%s", sel_knl_numa, sel_knl_mcdram)

    return write_features,node_class,special
end

function DerivedJobData:_get_request_array_task_count()
    local tmp = self.Request.array_inx
    local idx_start,idx_end = string.find(tmp, "%p")

    local task_count = 0
    local range_start = 0
    local range_end = 0
    local range_step = 1
    local in_range = false
    while idx_start do
        local before = string.match(tmp, "^%d+")
        local punc = string.sub(tmp, idx_start, idx_start)
        tmp = string.sub(tmp, idx_start + 1, -1)
        local after = string.match(tmp, "^%d+")

        if punc == "-" then
            in_range = true
            range_start = tonumber(before)
            range_end = tonumber(after)
        elseif punc == "," then
            if in_range then
                task_count = task_count +
                    math.ceil(((range_end + 1) - range_start) / range_step)
                range_step = 1
                in_range = false
            elseif before then
                task_count = task_count + 1
            end
        elseif punc == ":" then
            if in_range then
                range_step = tonumber(after)
            else
                -- ERROR
                local msg = string.format("Failed to parse job array \"%s\"", self.Request.array_inx)
                error(SlurmError:new(slurm.ERROR, msg, msg))
            end
        elseif punc == "%" then
            -- this is the concurrent task limit, no help
        else
            -- invalid character
            local msg = string.format("Failed to parse job array \"%s\"", self.Request.array_inx)
            error(SlurmError:new(slurm.ERROR, msg, msg))
        end

        idx_start,idx_end = string.find(tmp, "%p")
    end
    if in_range then
        task_count = task_count + math.ceil(((range_end + 1) - range_start) / range_step)
    else
        local final = string.match(tmp, "^%d+")
        if final then
            task_count = task_count + 1
        end
    end

    return task_count
end

function DerivedJobData:estimateCost(queue)

    -- even if job isn't an array PendingArrayTasks should return at least 1
    -- so we can assume non-zero here
    local n_array_tasks = self.PendingArrayTasks
    local n_cores = self:_calculate_core_resources(queue)
    local total_cores = n_array_tasks * n_cores
    local chargeFactor = queue:getChargeFactor() * self:getArchChargeFactor() * site.machineChargeFactor
    local timeLimit = self:getTimeLimit(queue) / 60

    -- TODO explicitly work out how NERSC will support unlimited time limit jobs (services)
    if timeLimit == nil then
        error(SlurmError:new(slurm.ERROR, "Could not determine job time limit", "Could not determine job time limit"))
    end
    return total_cores * chargeFactor * timeLimit
end

function DerivedJobData:apply_AdminCommentJson(value, auxvalue)
    if not self.ToWrite.AdminCommentJson then
        self.ToWrite.AdminCommentJson = {}
    end
    self.ToWrite.AdminCommentJson[value] = tostring(auxvalue)
end

function DerivedJobData:apply_AdminCommentJson_NoValue(value, auxvalue)
    if not self.ToWrite.AdminCommentJson then
        self.ToWrite.AdminCommentJson = {}
    end
    self.ToWrite.AdminCommentJson[value] = tostring(1)
end

function DerivedJobData:apply_JobSubmissionFailure(value, auxvalue)
    self.FailureReason = self.FailureReason or {}
    self.FailureReason[value] = true
end

function DerivedJobData:apply_JobSubmissionFailureAfterTime(value, auxvalue)
    self.FailureReason = self.FailureReason or {}
    if (os.time() > tonumber(auxvalue)) then
        self.FailureReason[value] = true
    end
end

function DerivedJobData:apply_ExecutionLicenses(value, auxvalue)
    local tgt_lic = value
    local tgt_lic_count
    local loc = string.find(tgt_lic, ":")
    if loc then
        tgt_lic = string.sub(value, 1, loc - 1)
        tgt_lic_count = tonumber(string.sub(value, loc + 1))
    else
        tgt_lic_count = 1
    end

    local licList = {}
    local licStr = nil
    for source in anonIter{self.ToWrite, self.Request, self.Record} do
        if source.licenses then
            licStr = source.licenses
            break
        end
    end

    if not licStr then
        licStr = ''
    end

    -- split out comma-separated license
    string.gsub(licStr, "[^,]+", function(substr) licList[#licList + 1] = substr end)

    local outlic = {}
    local found = 0
    for license in anonIter(licList) do
        loc = string.find(license, ":")
        local name = license
        local count
        if loc then
            name = string.sub(license, 1, loc - 1)
            count = tonumber(string.sub(license, loc + 1))
        else
            count = 1
        end

        if name == tgt_lic then
            count = tgt_lic_count
            found = 1
        end

        table.insert(outlic, string.format("%s:%d", name, count))
    end
    if found == 0 then
        table.insert(outlic, string.format("%s:%d", tgt_lic, tgt_lic_count))
    end

    local outlic_str = table.concat(outlic, ",")
    if (self.Record.licenses ~= outlic_str) or (self.Request.licenses and self.Request.licenses ~= outlic_str) then
        slurm.log_info("DerivedJob:apply_ExecutionLicenses: setting license to %s", outlic_str)
        self.Request.licenses = outlic_str
    end
end

-- END class DerivedJobData


local LogicalQueue = {}

function LogicalQueue:new(obj, label)
    obj = obj or {}

    obj.Apply = obj.Apply or {}
    obj.MatchingCriteria = obj.MatchingCriteria or {}
    obj.Requirements = obj.Requirements or {}
    obj.EvaluationPriority = obj.EvaluationPriority or 100
    obj.Label = label

    setmetatable(obj, self)
    self.__index = self

    return obj
end

function LogicalQueue._findString(str, target)
    str = str or "None"
    if target == nil then
        return false
    end
    if type(target) == "table" then
        for value in anonIter(target) do
            if str == value then return true end
        end
    elseif type(target) == "string" then
        return target == str
    end
    return false
end

function LogicalQueue._matchString(str, patterns)
    str = str or "None"
    if patterns == nil then
        return false
    end
    if type(patterns) == "table" then
        for pattern in anonIter(patterns) do
            local match = string.match(str, pattern)
            if match == str then return true end
        end
    elseif type(patterns) == "string" then
        local match = string.match(str, patterns)
        if match == str then return true end
    end
    return false
end

function LogicalQueue:_hasSufficientNerscBalance(job)
    local cost = job:estimateCost(self)
    local have_balance = false
    if not job.UserBalance or not job.AccountBalance then
        local msg = string.format("No available NERSC-hour balance information for user %s, account %s. Cannot proceed.", job.Username, job.Account)
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    slurm.log_info("job cost estimate: %0.2f, user balance: %0.2f, account balance: %0.2f", cost, job.UserBalance, job.AccountBalance)
    if cost <= job.UserBalance and cost <= job.AccountBalance then
        have_balance = true
    end
    return have_balance
end

function LogicalQueue:match_Account(value, job)
    return self._findString(job.Account, value)
end

function LogicalQueue:match_AccountRegexMatch(value, job)
    return self._matchString(job.Account, value)
end

function LogicalQueue:match_Arch(value, job)
    -- job.NodeClass should have the specified architecture (if any)
    return self._findString(job.NodeClass, value)
end

-- match_Exclusive is trying to tell if the user _specified_ --exclusive
function LogicalQueue:match_Exclusive(value, job)
    local is_exclusive = false
    local seen = false
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        local shared = source.shared
        if source.shared == slurm.NO_VAL16 then shared = nil end
        if shared ~= nil then
            is_exclusive = shared == slurm.JOB_SHARED_NONE
            seen = true
        end
    end

    -- WARNING: job.Record.shared is not actually implemented
    -- so we'll assume the partition is a good analog for user desire
    -- add in job.Record.shared so once we have a fix this logic is
    -- is automatically deprecated
    if not seen and job.Record.shared == nil and job.Record.partition then
        is_exclusive = job.PartList[job.Record.partition].max_share == 0
        seen = true
    end
    if not seen then
         -- this works because if we did not get a response then the user
         -- did not explicitly ask for exclusive.
         return seen == value
    end
    return is_exclusive == value
end

function LogicalQueue:match_MinNerscBalance(value, job)
    local balance = job.UserBalance
    if job.AccountBalance < balance then
        balance = job.AccountBalance
    end
    return value >= balance
end

function LogicalQueue:match_MinNodes(value, job)
    local nodes = job:_get_effective_nodes(self)
    local required_minimum = tonumber(value)
    return nodes >= required_minimum
end

function LogicalQueue:match_MaxNodes(value, job)
    local nodes = job:_get_effective_nodes(self)
    local required_maximum = tonumber(value)
    return nodes <= required_maximum
end

function LogicalQueue:match_SufficientNerscBalance(value, job)
    local have_balance = self:_hasSufficientNerscBalance(job)
    return value == have_balance
end

function LogicalQueue:match_RecordPartition(value, job)
    value = value or {}
    return self._findString(job.Record.partition, value)
end

function LogicalQueue:match_RecordPartitionRegexMatch(value, job)
    value = value or {}
    return self._matchString(job.Record.partition, value)
end

function LogicalQueue:match_RecordQos(value, job)
    value = value or {}
    return self._findString(job.Record.qos, value)
end

function LogicalQueue:match_RecordQosRegexMatch(value, job)
    value = value or {}
    return self._matchString(job.Record.qos, value)
end

function LogicalQueue:match_RequestPartition(value, job)
    value = value or {}
    return self._findString(job.Request.partition, value)
end

function LogicalQueue:match_RequestPartitionRegexMatch(value, job)
    value = value or {}
    return self._matchString(job.Request.partition, value)
end

function LogicalQueue:match_RequestQos(value, job)
    value = value or {}
    return self._findString(job.Request.qos, value)
end

function LogicalQueue:match_RequestQosRegexMatch(value, job)
    value = value or {}
    return self._matchString(job.Request.qos, value)
end

function LogicalQueue:match_RequestAdvancedReservation(value, job)
    value = value or {}
    local reqReservation = false
    local haveReservation = false
    if value ~= nil and value then
        reqReservation = true
    end
    if job.Request.reservation and not site.systemReservations[job.Request.reservation] then
        haveReservation = true
    end
    return reqReservation == haveReservation
end

function LogicalQueue:match_RecordAdvancedReservation(value, job)
    value = value or {}
    local reqReservation = false
    local haveReservation = false
    if value ~= nil and value then
        reqReservation = true
    end
    if job.Record.reservation and not site.systemReservations[job.Record.reservation] then
        haveReservation = true
    end
    return reqReservation == haveReservation
end

-- Match if time_min (--time-min) is set and is at most value
-- value should be an integer and in units of minutes to match time_min
function LogicalQueue:match_RecordMaximumTimeMin(value, job)
    if not job.Record.time_min or job.Record.time_min == slurm.NO_VAL then
        return false
    end
    return job.Record.time_min <= value
end
function LogicalQueue:match_RequestMaximumTimeMin(value, job)
    if not job.Request.time_min or job.Request.time_min == slurm.NO_VAL then
        return false
    end
    return job.Request.time_min <= value
end
function LogicalQueue:match_MaximumTimeMin(value, job)
    local time_min = nil
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        if source['time_min'] and source['time_min'] ~= slurm.NO_VAL then
            time_min = source['time_min']
            break
        end
    end
    if not time_min then
        return false
    end
    return time_min <= value
end

-- Match if time_limit (--time) is explicitly set by user (no partition default) and is at most value
-- value should be an integer and in units of minutes to match time_limit
function LogicalQueue:match_RecordMaximumTimeLimit(value, job)
    if not job.Record.time_limit or job.Record.time_limit == slurm.NO_VAL then
        return false
    end
    return job.Record.time_limit <= value
end
function LogicalQueue:match_RequestMaximumTimeLimit(value, job)
    if not job.Request.time_limit or job.Request.time_limit == slurm.NO_VAL then
        return false
    end
    return job.Request.time_limit <= value
end

-- Match if the time_limit (--time) from any source (including partition default) is at most value
-- value should be an integer and in units of minutes to match time_limit
function LogicalQueue:match_MaximumTimeLimit(value, job)
    local timelimit = job:getTimeLimit(self)
    if not timelimit or timelimit == slurm.NO_VAL then
        return false
    end
    return timelimit <= value
end

-- Match if the time_limit (--time) from any source (including partition default) is at least value
-- value should be an integer and in units of minutes to match time_limit
function LogicalQueue:match_MinimumTimeLimit(value, job)
    local timelimit = job:getTimeLimit(self)
    if not timelimit or timelimit == slurm.NO_VAL then
        return false
    end
    return timelimit >= value
end

function LogicalQueue:validate_RequireArchSpecified(value, job)
    if value and not job.NodeClass then
        local msg = string.format("No hardware architecture specified (-C)!")
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:validate_RequireExclusive(value, job)
    return match_Exclusive(value, job)
end

function LogicalQueue:validate_RequireSufficientNerscBalance(value, job)
    local have_balance = self:_hasSufficientNerscBalance(job)
    local cost = job:estimateCost(self)
    if value == have_balance then
        return true
    end
    if have_balance then
        local err_msg = string.format("The %s logical queue requires an " ..
                        "lower balance than the estimated job cost. " ..
                        "Job cost estimated at %0.2f " ..
                        "NERSC-Hours, your balance is %0.2f NERSC-Hours " ..
                        "(Repo: %0.2f NERSC-Hours). Cannot proceed, please " ..
                        "see https://docs.nersc.gov/jobs/policy/ for your " ..
                        "options to run this job.", self.Label, cost,
                        job.UserBalance, job.AccountBalance)
        error(SlurmError:new(slurm.ERROR, err_msg, err_msg))
    else
        local err_msg = string.format("Job cost estimated at %0.2f " ..
                        "NERSC-Hours, your balance is %0.2f NERSC-Hours " ..
                        "(Repo: %0.2f NERSC-Hours). Cannot proceed, please " ..
                        "see https://docs.nersc.gov/jobs/policy/ for your " ..
                        "options to run this job.", cost, job.UserBalance,
                        job.AccountBalance)
        error(SlurmError:new(slurm.ERROR, err_msg, err_msg))
    end
    return false
end

function LogicalQueue:validate_IfArchKnlAllowedNumaModes(value, job)
    if job.NodeClass ~= "knl" then
        return true
    end

    local features = nil
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        if source['features'] then
            features = source['features']
            break
        end
    end

    local knl_mode = {'quad','snc2','snc4','hemi','a2a'}
    local sel_knl_mode = nil
    for idx = 1, #knl_mode do
        if string.find(features, knl_mode[idx]) ~= nil then
            sel_knl_mode = knl_mode[idx]
        end
    end
    if self._findString(sel_knl_mode, value) then
        return true
    end
    local avail_mode = value
    if type(value) == 'table' then
        avail_mode = table.concat(value, ',')
    end
    local msg = string.format("Selected policy only allows knl numa mode(s): %s", avail_mode)

    error(SlurmError:new(slurm.ERROR, msg, msg))
end

function LogicalQueue:validate_IfArchKnlAllowedMcdramModes(value, job)
    if job.NodeClass ~= "knl" then
        return true
    end

    local features = nil
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        if source['features'] then
            features = source['features']
            break
        end
    end

    local knl_mode = {'flat','split','equal','cache'}
    local sel_knl_mode = nil
    for idx = 1, #knl_mode do
        if string.find(features, knl_mode[idx]) ~= nil then
            sel_knl_mode = knl_mode[idx]
        end
    end
    if self._findString(sel_knl_mode, value) then
        return true
    end
    local avail_mode = value
    if type(value) == 'table' then
        avail_mode = table.concat(value, ',')
    end
    local msg = string.format("Selected policy only allows knl mcdram mode(s): %s", avail_mode)

    error(SlurmError:new(slurm.ERROR, msg, msg))
end

-- Lua function to check if a job is a batch job
function _is_batch(job)
    local is_batch = (job.Request.script ~= nil and job.Request.script ~= '')
    return is_batch
end

function LogicalQueue:validate_RequireInteractive(value, job)
    local is_batch = _is_batch(job)
    slurm.log_debug("is_batch: %s, %s", tostring(is_batch), tostring(job.Request.script))
    if value and is_batch then
       local msg = string.format("Cannot submit batch jobs to %s.", self.Label)
       error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:validate_ForbidInteractive(value, job)
    local is_batch = _is_batch(job)
    slurm.log_debug("is_batch: %s, %s", tostring(is_batch), tostring(job.Request.script))
    if value and not is_batch then
       local msg = string.format("Cannot submit interactive jobs to %s.", self.Label)
       error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:validate_RequireMaxCpuPerNodeFraction(value, job)
    slurm.log_error("WARNING: use of deprecated function validate_RequireMaxCpuPerNodeFraction")
    return self:validate_RequireMaxNode(value, job)
end

function LogicalQueue:validate_RequireMaxNode(value, job)
    local lval = tonumber(value)
    local effective_nodes = job:_get_effective_nodes(self)

    slurm.log_info("validate_RequireMaxNode: effective_nodes: %s, limit: %s", tostring(effective_nodes), tostring(lval))
    if effective_nodes > lval then
        local msg = string.format("More resources requested than allowed for logical queue %s (%s requested core-equivalents > %s)", self.Label, tostring(effective_nodes), tostring(lval))
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:validate_RequireArch(value, job)
    local arch = job.NodeClass
    local apply = self.Apply or {}
    arch = arch or apply.ExecutionArch

    return arch == value
end

function LogicalQueue:validate_RequireMaximumTimeMin(value, job)
    local time_min = nil
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        if source['time_min'] and source['time_min'] ~= slurm.NO_VAL then
            time_min = source['time_min']
            break
        end
    end
    if not time_min or time_min > value then
        local msg = string.format("Logical queue %s requires --time-min to be specified and at most %d minutes.", self.Label, value)
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:validate_RequireMaximumTimeLimit(value, job)
    local time_limit = job:getTimeLimit(self)
    if not time_limit or time_limit > value then
        local msg = string.format("Logical queue %s requires --time to be at most %d minutes.", self.Label, value)
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    return true
end

function LogicalQueue:apply_ExecutionArch(value, job)
    -- NOTE: ExecutionArch is NOT appropriate for complex architectures like KNL at present

    if job.NodeClass and job.NodeClass ~= value then
        local msg = string.format("Requested policies require the %s architecture, but you have requested %s. Rejecting request.", value, job.NodeClass)
        error(SlurmError:new(slurm.ERROR, msg, msg))
    end
    -- TODO add something to NerscConfig to transform NodeClass (arch) to writeable features based on arch defaults
    if (job.Record.features ~= value) or (job.Request.features and job.Request.features ~= value) then
        slurm.log_info("apply_ExecutionArch (LogicalQueue:%s): setting features to %s", self.Label, value)
        job.Request.features = value
    end
end

function LogicalQueue:apply_ExecutionPartition(value, job)
    if (job.Record.partition ~= value) or (job.Request.partition and job.Request.partition ~= value) then
        slurm.log_info("apply_ExecutionPartition (LogicalQueue:%s): setting partition to %s", self.Label, value)
        job.Request.partition = value
    end
end

function LogicalQueue:apply_ExecutionQos(value, job)
    -- this will get set elsewhere if append account is enabled
    if self.Apply.ExecutionQosAppendAccount then return end

    if (job.Record.qos ~= value) or (job.Request.qos and job.Request.qos ~= value) then
        slurm.log_info("apply_ExecutionQos (LogicalQueue:%s): setting qos to %s", self.Label, value)
        job.Request.qos = value
    end
end

function LogicalQueue:apply_ExecutionQosAppendAccount(value, job)
    if not value then return end

    local qos = self.Apply.ExecutionQos .. "_" .. job.Account
    if (job.Record.qos ~= qos) or (job.Request.qos and job.Request.qos ~= qos) then
        slurm.log_info("apply_ExecutionQosAppendAccount (LogicalQueue:%s): setting qos to %s", self.Label, qos)
        job.Request.qos = qos
    end
end

function LogicalQueue:apply_ExecutionGres(value, job)
    local tgt_gres = value
    local tgt_gres_count
    local loc = string.find(tgt_gres, ":")
    if loc then
        tgt_gres = string.sub(value, 1, loc - 1)
        tgt_gres_count = tonumber(string.sub(value, loc + 1))
    else
        tgt_gres_count = 1
    end

    local gresList = {}
    local gresStr = nil
    for source in anonIter{job.ToWrite, job.Request, job.Record} do
        if source.gres then
            gresStr = source.gres
            break
        end
    end

    if not gresStr then
        gresStr = ''
    end

    -- split out comma-separated gres
    string.gsub(gresStr, "[^,]+", function(substr) gresList[#gresList + 1] = substr end)

    local outgres = {}
    for gres in anonIter(gresList) do
        loc = string.find(gres, ":")
        local name = gres
        local count
        if loc then
            name = string.sub(gres, 1, loc - 1)
            count = tonumber(string.sub(gres, loc + 1))
        else
            count = 1
        end

        if name == tgt_gres then
            count = tgt_gres_count
        end

        table.insert(outgres, string.format("%s:%d", name, count))
    end

    local outgres_str = table.concat(outgres, ",")
    if (job.Record.gres ~= outgres_str) or (job.Request.gres and job.Request.gres ~= outgres_str) then
        slurm.log_info("apply_ExecutionGres (LogicalQueue:%s): setting gres to %s", self.Label, outgres_str)
        job.Request.gres = outgres_str
    end
end

function LogicalQueue:apply_ExecutionLicenses(value, job)
    slurm.log_info("apply_ExecutionLicenses (LogicalQueue:%s): calling DerivedJob:apply_ExecutionLicneses with %s", tostring(self.Label), tostring(value))
    job:apply_ExecutionLicenses(value, nil)
end

function LogicalQueue:apply_ExecutionGresSingleNode(value, job)
    local nodes = job:_get_effective_nodes(self)
    if max_node == 1 then
        self:apply_ExecutionGres(value, job)
    end
end

function LogicalQueue:_eval_cond(prefix, key, value, job)
    local logicalnot = false
    local k = key
    local ret = false

    if string.sub(k, 1, 1) == "~" then
        logicalnot = true
        k = string.sub(k, 2)
    end

    local func = prefix .. "_" .. k
    if self[func] ~= nil then
        -- have to include self in argument-list here because
        -- the function is called directly instead of using the
        -- colon operator syntax
        ret = self[func](self, value, job)
        if logicalnot then
            ret = not ret
        end
    else
        slurm.log_error("WARNING: " .. k .. " not implemented")
    end
    return ret
end

function LogicalQueue:_remove_from_defaults(key, defaults)
    local lkey = key
    if string.sub(lkey, 1, 1) == "~" then
        lkey = string.sub(lkey, 2)
    end
    if defaults[lkey] then defaults[lkey] = nil end
    if defaults["~" .. lkey] then defaults["~" .. lkey] = nil end
end


function LogicalQueue:evaluate(job)
    local anymatch = false
    job = job or {}
    job.Request = job.Request or {}
    job.Record = job.Record or {}

    local ref_queue_defaults = site.queue_defaults or {}
    local ref_default_crit = ref_queue_defaults.MatchingCriteria or {}
    if #self.MatchingCriteria == 0 then
        return true
    end
    for criteria in anonIter(self.MatchingCriteria) do
        local ismatch = true

        if anymatch then return true end

        -- setup defaults from reference (each time)
        local defaults = {}
        for k,v in pairs(ref_default_crit) do
            defaults[k] = v
        end

        -- review all criteria
        for k,v in pairs(criteria) do
            if not ismatch then break end
            if k and v ~= nil then
                ismatch = ismatch and self:_eval_cond("match", k, v, job)
            end
            self:_remove_from_defaults(k, defaults)
        end
        for k,v in pairs(defaults) do
            if not ismatch then break end
            if k and v ~= nil then
                ismatch = ismatch and self:_eval_cond("match", k, v, job)
            end
        end
        anymatch = anymatch or ismatch
    end
    return anymatch
end

function LogicalQueue:validate(job)
    local ismatch = true

    job = job or {}
    job.Request = job.Request or {}
    job.Record = job.Record or {}

    local ref_queue_defaults = site.queue_defaults or {}
    local ref_default_req = ref_queue_defaults.Requirements or {}

    -- copy defaults from reference
    local defaults = {}
    for k,v in pairs(ref_default_req) do
        defaults[k] = v
    end
    for k,v in pairs(self.Requirements) do
        if not ismatch then break end
        if k ~= nil and v ~= nil then
            ismatch = ismatch and self:_eval_cond("validate", k, v, job)
        end
        self:_remove_from_defaults(k, defaults)
    end
    for k,v in pairs(defaults) do
        if not ismatch then break end
        if k ~= nil and v ~= nil then
            ismatch = ismatch and self:_eval_cond("validate", k, v, job)
        end
    end
    return ismatch
end

function LogicalQueue:isNodeExclusive(part_info)
    local part = self:getExecutionPartition()
    return part_info[part].max_share == 0
end

function LogicalQueue:getChargeFactor()
    return self.chargeFactor or 1
end

function LogicalQueue:getExecutionPartition()
    local queue_defaults = site.queue_defaults or {}
    local apply_defs = queue_defaults.Apply or {}
    for source in anonIter{self.Apply, apply_defs} do
        if source['ExecutionPartition'] then
            return source['ExecutionPartition']
        end
    end
    local err_msg = string.format("Selected queue %s definition has no execution partition", self.Label)
    error(SlurmError:new(slurm.ERROR, err_msg, err_msg))
end

function LogicalQueue:doesRequireNerscBalance()
    if self.RequireNerscBalance ~= nil and not self.RequireNerscBalance then
        return false
    end
    if self.RequireNimBalance ~= nil and not self.RequireNimBalance then
        return false
    end
    return true
end

-- END class LogicalQueue

-- Class NerscConfig --
local NerscConfig = {}

function NerscConfig:new(obj)
    obj.system = obj.system or "(undefined site)"
    obj.machineChargeFactor = obj.machineChargeFactor or 1.0
    obj.queues = obj.queues or {}
    obj.arch = obj.arch or {}
    obj.systemReservations = obj.systemReservations or {}
    obj.queues_by_prio = nil
    obj.spank = obj.spank or {}
    obj.errorMessages = obj.errorMessages or {}

    setmetatable(obj, self)
    self.__index = self

    obj:_setup_queues()
    if obj:validate() then
        return obj
    end

    -- failure to validate should for an error()
    -- should never get here
    return nil
end

function NerscConfig:_add_queue(queue)
    local mt = getmetatable(queue)
    if mt ~= LogicalQueue then
        error("Called NerscConfig:append_queue with non-LogicalQueue argument", 2)
    end

    if not self.isTestHarness then
        if not queue.Apply.ExecutionPartition then
            local msg = string.format("%s queue missing ExecutionPartition", queue.Label)
            error(msg, 2)
        end
        if not queue.Apply.ExecutionQos then
            local msg = string.format("%s queue missing ExecutionQos", queue.Label)
            error(msg, 2)
        end
    end

    -- add or replace queue
    self.queues[queue.Label] = queue
end

function NerscConfig:add_queue(queue)
    self:_add_queue(queue)
    self:_sort_queues()
end

function NerscConfig:_setup_queues()
    local queues = self.queues or {}
    self.queues = {}
    for key,value in pairs(queues) do
        value = value or {}
        if type(value) ~= "table" then
            error(string.format("%s entry in proposed queues is not a table", tostring(key)))
        end

        local name = value.Label or key
        name = tostring(name)
        if self.queues[name] then
            error(string.format("queue %s already exists", name))
        end

        local q = LogicalQueue:new(value, name)
        self:_add_queue(q)
        slurm.log_info("NerscConfig:_setup_queues: added queue %s", name)
    end
    self:_sort_queues()
    for key,value in pairs(self.queues_by_prio) do
        slurm.log_info("NerscConfig:_setup_queues: queue %d: %s, %d", key, value.Label, value.EvaluationPriority)
    end
end

function NerscConfig:_sort_queues()
    self.queues_by_prio = {}
    for q in anonIter(self.queues) do
        table.insert(self.queues_by_prio, q)
    end

    table.sort(self.queues_by_prio,
                function (a, b)
                    local a_eval = a.EvaluationPriority or 0
                    local b_eval = b.EvaluationPriority or 0
                    local a_label = a.Label or "Unknown"
                    local b_label = b.Label or "Unknown"
                    if a_eval == b_eval then return a_label < b_label end
                    return a_eval > b_eval
                end
    )
end

function NerscConfig:validate()
    if self.isTestHarness then return true end

    if not self.machineChargeFactor then
        slurm.log_error("Machine Charge Factor (machineChargeFactor) not defined")
        return false
    end
    if #self.queues_by_prio == 0 then
        slurm.log_error("No Logical Queues defined (queues list)")
        return false
    end

    -- make sure all needed evaluation functions exist
    local match_keys = {}
    for _queue in anonIter(self.queues) do
        for i = 1,#_queue.MatchingCriteria do
            local crit = _queue.MatchingCriteria[i]
            for key,value in pairs(crit) do
                if string.sub(key, 1, 1) == "~" then
                    key = string.sub(key, 2)
                end
                match_keys[key] = true
            end
        end
    end
    return true
end


-- END Class NerscConfig --

local function _process_job(job_request, job_record, part_info, uid, operation)
    --if uid == 0 then
    --    return slurm.SUCCESS
    --end
    job_request = job_request or {}
    job_record = job_record or {}

    -- stage 0: screen out restricted fields
    if job_request.nice and job_request.nice < 2147483648 then
        error(SlurmError:new(2002, "Not allowed to specify negative nice values at NERSC",
                             string.format("User attempted to set nice to %d", job_request.nice)))
    end

    -- stage 1: integrate job data
    local job = DerivedJobData:new(job_request, job_record, part_info, operation)
    if not job then return slurm.ERROR end

    -- stage 2: evaluate queues
    local evaluated = {}
    local selected_queue = nil
    for qdata in anonIter(site.queues_by_prio) do
        slurm.log_info("evaluating queue %s", qdata.Label)
        local ok = qdata:evaluate(job)
        if ok then
            -- validate matching queues
            local status,err = pcall( function() qdata:validate(job) end )
            if status then
                selected_queue = qdata
            else
                table.insert(evaluated, {qdata, err})
            end
            break
        end
    end

    -- stage 3: generate an error if there were no successfully validated queues
    if not selected_queue then
        if #evaluated > 0 then
            local qdata = evaluated[1][1]
            local err = evaluated[1][2]
            error(err)
        else
            local msg = "Job request does not match any supported policy for " .. site.system
            error(SlurmError:new(slurm.ERROR, msg, msg))
        end
    end

    -- stage 4: check on accounting
    local timeLimit = job:getTimeLimit(selected_queue)
    slurm.log_info("job time limit: %s", tostring(timeLimit))
    if selected_queue:doesRequireNerscBalance() then
        selected_queue:validate_RequireSufficientNerscBalance(true, job)
    end

    -- stage 5: apply changes
    job:apply(selected_queue)

    -- stage 6: check final failure modes
    for reason,value in pairs(job.FailureReason) do
        local item = 'IgnoreFailureReason_' .. tostring(reason)
        if selected_queue[item] == nil then
             local msg = site.errorMessages[reason]
             if not msg then
                 msg = "Job submission failed with reason: " .. tostring(reason)
             end
             error(SlurmError:new(slurm.ERROR, msg, msg))
        end
    end

    -- accept
    return slurm.SUCCESS
end

local function process_job(job_request, job_record, part_info, uid, operation)
    local rc = slurm.SUCCESS
    local status,err = pcall(function () _process_job(job_request, job_record, part_info, uid, operation) end)
    if not status then
        rc = slurm.ERROR
        if type(err) == "table" then
            if err.code then rc = err.code end
            if err.userMsg and err.userMsg ~= "" then
                slurm.log_user("%s", err.userMsg)
            end
            if err.slurmMsg and err.slurmMsg ~= "" then
                slurm.log_error("%s", err.slurmMsg)
            end
        else
            slurm.log_user("Job submission NERSC-local logic failed. This is probably a bug. Debugging information for this incident has been logged, but please contact consult@nersc.gov if you wish to provide further information or get assistance.")
            slurm.log_error("%s", tostring(err))
        end
    end
    return rc
end

local function slurm_job_submit(job_request, part_info, submit_uid)
    slurm.log_info("starting job_submit")
    return process_job(job_request, nil, part_info, submit_uid, "submit")
end

local function slurm_job_modify(job_request, job_record, part_info, submit_uid)
    slurm.log_info("starting job_modify")
    return process_job(job_request, job_record, part_info, submit_uid, "modify")
end

local function setupVariables(_site, _isTestHarness)
    _site.isTestHarness = _isTestHarness
    site = NerscConfig:new(_site)

    return site
end

local function setupFromYaml(fname)
    local _site = yaml.loadpath(fname)
    return setupVariables(_site, false)
end

jsl.LogicalQueue = LogicalQueue
jsl.DerivedJobData = DerivedJobData
jsl.SlurmError = SlurmError
jsl.NerscConfig = NerscConfig
jsl.setupVariables = setupVariables
jsl.setupFromYaml = setupFromYaml

_G.slurm_job_submit = slurm_job_submit
_G.slurm_job_modify = slurm_job_modify

return jsl
