--[[
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
]]--

isTestHarness = true
slurm = require "slurm"

local jsl = require "lib_job_submit"

package.path = package.path .. ';bower_components/luaunit/luaunit.lua'
local lu = require("luaunit")

-- zero out nersc namespace
nersc = {}


slurm.log_info("test %d", 5)
function _setup_job(job_request, job_record, part_list)
    job_record = job_record or {}
    job_record._test_harness = true

    return jsl.DerivedJobData:new(job_request, job_record, part_list)
end

TestJobSubmission = {}
function TestJobSubmission:test_slurm_job_submit()
    local _nersc = {}
    local debugq = jsl.LogicalQueue:new({}, "debug")
    debugq.Apply.ExecutionPartition = "debug"
    debugq.Apply.ExecutionQos = "debug"
    _nersc.machineChargeFactor = 1.0
    _nersc.nerscSlurmPluginDir = "."
    _nersc.machineChargeFactor = 1.0
    _nersc.queues = {}
    table.insert(_nersc.queues, debugq)
    _nersc.arch = {}
    _nersc.arch.haswell = {}
    _nersc.arch.haswell.enabled = true
    _nersc.arch.haswell.cpus_per_core = 2
    _nersc.arch.haswell.cores_per_socket = 16
    _nersc.arch.haswell.sockets_per_node = 2
    _nersc.arch.haswell.default_cpus_per_task = 2
    _nersc.default_arch = "haswell"

    local nersc = jsl.setupVariables(_nersc, false)
    lu.assertEquals(nersc, _nersc)
    local job_req = {}
    local part_list = {}

    job_req.cpus_per_task = slurm.NO_VAL16
    job_req.ntasks_per_core = slurm.NO_VAL16
    job_req.ntasks_per_socket = slurm.NO_VAL16
    job_req.ntasks_per_node = slurm.NO_VAL16

    part_list.debug = {}
    part_list.debug.default_time = 5
    part_list.debug.max_time = 30
    part_list.debug.max_share = 0
    part_list.regular = {}
    part_list.regular.default_time = 5
    part_list.regular.max_time = 2880
    part_list.regular.max_share = 0

    -- make sure job fails when user info is broken
    local result = slurm_job_submit(job_req, part_list, 0)
    lu.assertEquals(result, slurm.ERROR)

    job_req.user_name = "normalUser"
    job_req.account = "normalAccount"
    -- basic submit
    print("start successful")
    local limit = 1
    local timer = os.clock()
    for i = 1,limit do
    result = slurm_job_submit(job_req, part_list, 1234)
    lu.assertEquals(result, slurm.SUCCESS)
    end
    timer = (os.clock() - timer) / limit
    print("time to submit one job: ", timer)

    job_req.user_name = "poorUser"
    job_req.min_nodes = 10
    result = slurm_job_submit(job_req, part_list, 1234)
    lu.assertEquals(result, slurm.ERROR)

    job_req.account = "poorAccount"
    result = slurm_job_submit(job_req, part_list, 1234)
    lu.assertEquals(result, slurm.ERROR)

    local overrunq = jsl.LogicalQueue:new({}, "overrun")
    overrunq.MatchingCriteria = {}
    overrunq.MatchingCriteria[1] = {}
    overrunq.MatchingCriteria[1].RequestQos = "overrun"
    overrunq.Apply.ExecutionPartition = "regular"
    overrunq.Apply.ExecutionQos = "overrun"
    overrunq.RequireNerscBalance = false
    overrunq.EvaluationPriority = 9999
    nersc:add_queue(overrunq)

    job_req.qos = "overrun"
    result = slurm_job_submit(job_req, part_list, 1234)
    lu.assertEquals(result, slurm.SUCCESS)
end
-- END class TestJobSubmission

TestLogicalQueue = {} -- class

    function TestLogicalQueue:test_checkreq()
        local nersc = jsl.setupVariables({}, true)
        local label,queue,ret
        local request = {}
        local record = {}
        local job = {}
        job.Request = request
        job.Record = record

        job.NodeClass = "knl"
        job.Account = "nstaff"
        local queue1 = {}
        queue1.MatchingCriteria = {}
        queue1.Requirements = {}
        queue1.Requirements.RequireArchSpecified = true

        queue1 = jsl.LogicalQueue:new(queue1, "debug")
        ret = queue1:validate(job)
        lu.assertEquals(ret, true)
    end

    function TestLogicalQueue:test_checkcriteria()
        local nersc = jsl.setupVariables({}, true)
        local label,queue,ret
        local request = {}
        local record = {}
        local job = {}
        job.Request = request
        job.Record = record
        job.NodeClass = "knl"
        job.Account = "nstaff"
        local queue1 = {}
        queue1.MatchingCriteria = {}
        queue1.MatchingCriteria[1] = {}
        queue1.MatchingCriteria[1].RequestQos = { "debug", "None" }
        queue1.MatchingCriteria[2] = {}
        queue1.MatchingCriteria[2].RecordQos = { "debug" }
        queue1.MatchingCriteria[2].RequestQos = { "None" }

        queue1 = jsl.LogicalQueue:new(queue1, "debug")

        ret = queue1:evaluate(job)
        lu.assertEquals(ret, true)

        request.qos = "regular"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, false)

        request.qos = "debug"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, true)

	request.qos = "exvivo"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, false)

        request.qos = nil
        record.qos = "debug"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, true)

        nersc.queue_defaults = {}
        nersc.queue_defaults.MatchingCriteria = {}
        nersc.queue_defaults.MatchingCriteria.AccountRegexMatch = 'jgi_.*'

        ret = queue1:evaluate(job)
        lu.assertEquals(ret, false)

        job.Account = "jgi_test"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, true)

        -- ensure that queue matching criteria exceed defaults
        queue1.MatchingCriteria[1].AccountRegexMatch = 'nstaff'
        queue1.MatchingCriteria[2].AccountRegexMatch = 'nstaff'
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, false)

        job.Account = "nstaff"
        ret = queue1:evaluate(job)
        lu.assertEquals(ret, true)
    end

    function TestLogicalQueue:test_matchAccount()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")

        local ok_accounts = {}
        table.insert(ok_accounts, "test")
        table.insert(ok_accounts, "test2")

        local job = {}
        job.Account = "test"

        lu.assertEquals(queue1:match_Account("test", job), true)
        lu.assertEquals(queue1:match_Account("test2", job), false)
        lu.assertEquals(queue1:match_Account(ok_accounts, job), true)
        job.Account = "test2"
        lu.assertEquals(queue1:match_Account(ok_accounts, job), true)
        job.Account = "test3"
        lu.assertEquals(queue1:match_Account(ok_accounts, job), false)
    end

    function TestLogicalQueue:test_matchAccountRegexMatch()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")

        local ok_accounts = {}
        table.insert(ok_accounts, "test")
        table.insert(ok_accounts, "test2")

        local job = {}
        job.Account = "test"
        lu.assertEquals(queue1:match_AccountRegexMatch("test", job), true)
        lu.assertEquals(queue1:match_AccountRegexMatch("test2", job), false)
        lu.assertEquals(queue1:match_AccountRegexMatch("test.*", job), true)
        lu.assertEquals(queue1:match_AccountRegexMatch(ok_accounts, job), true)
        job.Account = "test2"
        lu.assertEquals(queue1:match_AccountRegexMatch(ok_accounts, job), true)

        job.Account = "test3"
        lu.assertEquals(queue1:match_AccountRegexMatch(ok_accounts, job), false)

        table.insert(ok_accounts, "test%d+")
        lu.assertEquals(queue1:match_AccountRegexMatch(ok_accounts, job), true)
    end

    function TestLogicalQueue:test_match_Arch()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")

        local job = {}
        job.NodeClass = "knl"

        local ok_nodeclass = {}
        table.insert(ok_nodeclass, "knl")
        table.insert(ok_nodeclass, "haswell")

        lu.assertEquals(queue1:match_Arch("knl", job), true)
        lu.assertEquals(queue1:match_Arch("haswell", job), false)
        lu.assertEquals(queue1:match_Arch(ok_nodeclass, job), true)

        job.NodeClass = "haswell"
        lu.assertEquals(queue1:match_Arch(ok_nodeclass, job), true)
        job.NodeClass = "ivybridge"
        lu.assertEquals(queue1:match_Arch(ok_nodeclass, job), false)
        lu.assertEquals(queue1:match_Arch("None", job), false)

        job = {}
        lu.assertEquals(queue1:match_Arch(ok_nodeclass, job), false)
        lu.assertEquals(queue1:match_Arch("None", job), true)
    end

    function TestLogicalQueue:test_match_MinMaxNodes()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")
        queue1.Apply.ExecutionPartition = "debug"

        -- set partition max
        local part_list = {}
        part_list.debug = {}
        part_list.debug.max_time = 30

        local job_req = {}
        local job_rec = {}
        local job = _setup_job(job_req, job_rec, part_list)

        job_req.cpus_per_task = slurm.NO_VAL16
        job_req.ntasks_per_core = slurm.NO_VAL16
        job_req.ntasks_per_socket = slurm.NO_VAL16
        job_req.ntasks_per_node = slurm.NO_VAL16

        part_list.debug = {}
        part_list.debug.default_time = 5
        part_list.debug.max_time = 30
        part_list.debug.max_share = 0

        local sharedq = jsl.LogicalQueue:new({}, "shared")
        sharedq.Apply.ExecutionPartition = "shared"
        part_list.shared = {}
        part_list.shared.default_time = 5
        part_list.shared.max_time = 720
        part_list.shared.max_share = 1
        part_list.shared.def_mem_per_cpu = 1952 * 1024 * 1024

        nersc.arch = {}
        nersc.arch.haswell = {}
        nersc.arch.haswell.enabled = true
        nersc.arch.haswell.cpus_per_core = 2
        nersc.arch.haswell.cores_per_socket = 16
        nersc.arch.haswell.sockets_per_node = 2
        nersc.arch.haswell.default_cpus_per_task = 2
        nersc.default_arch = "haswell"

        -- no input
        lu.assertEquals(queue1:match_MinNodes(1, job), true)
        lu.assertEquals(queue1:match_MinNodes(2, job), false)
        lu.assertEquals(queue1:match_MaxNodes(1, job), true)

        -- explicit -N
        job_req.max_nodes = 5
        job_req.min_nodes = 3

        -- checking for worse case scenario, so should always behave as 5 nodes
        lu.assertEquals(queue1:match_MinNodes(1, job), true)
        lu.assertEquals(queue1:match_MinNodes(3, job), true)
        lu.assertEquals(queue1:match_MinNodes(5, job), true)
        lu.assertEquals(queue1:match_MinNodes(6, job), false)

        lu.assertEquals(queue1:match_MaxNodes(1, job), false)
        lu.assertEquals(queue1:match_MaxNodes(3, job), false)
        lu.assertEquals(queue1:match_MaxNodes(5, job), true)
        lu.assertEquals(queue1:match_MaxNodes(6, job), true)
    end

    function TestLogicalQueue:test_match_MaximumTimeMin()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")

        local job = {}
        -- assure unspecified time_min fails
        job.Request = {}
        job.Record = {}
        lu.assertEquals(queue1:match_MaximumTimeMin(100, job), false)

        -- check that Request-specific and generic can see request time
        job.Request = {}
        job.Record = {}
        job.Request.time_min = 50
        lu.assertEquals(queue1:match_MaximumTimeMin(100, job), true)
        lu.assertEquals(queue1:match_RequestMaximumTimeMin(100, job), true)
        lu.assertEquals(queue1:match_RecordMaximumTimeMin(100, job), false)

        -- check that Record-specific and generic can see record time
        job.Request = {}
        job.Record = {}
        job.Record.time_min = 50
        lu.assertEquals(queue1:match_MaximumTimeMin(100, job), true)
        lu.assertEquals(queue1:match_RequestMaximumTimeMin(100, job), false)
        lu.assertEquals(queue1:match_RecordMaximumTimeMin(100, job), true)

        -- check that time_min can be exceeded
        job.Request = {}
        job.Record = {}
        job.Record.time_min = 125
        lu.assertEquals(queue1:match_MaximumTimeMin(100, job), false)
        lu.assertEquals(queue1:match_RequestMaximumTimeMin(100, job), false)
        lu.assertEquals(queue1:match_RecordMaximumTimeMin(100, job), false)

        -- check that generic prioritizes Request response
        job.Request = {}
        job.Record = {}
        job.Request.time_min = 75
        job.Record.time_min = 125
        lu.assertEquals(queue1:match_MaximumTimeMin(100, job), true)
        lu.assertEquals(queue1:match_RequestMaximumTimeMin(100, job), true)
        lu.assertEquals(queue1:match_RecordMaximumTimeMin(100, job), false)
    end

    function TestLogicalQueue:test_validate_RequireMaximumTimeMin()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")

        local job = {}
        -- assure unspecified time_min fails
        job.Request = {}
        job.Record = {}
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeMin(100, job) end )
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Logical queue test requires --time-min to be specified and at most 100 minutes.")

        -- check that Request-specific and generic can see request time
        job.Request = {}
        job.Record = {}
        job.Request.time_min = 50
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeMin(100, job) end )
        lu.assertEquals(status, true)

        -- check that Record-specific and generic can see record time
        job.Request = {}
        job.Record = {}
        job.Record.time_min = 50
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeMin(100, job) end )
        lu.assertEquals(status, true)

        -- check that time_min can be exceeded
        job.Request = {}
        job.Record = {}
        job.Record.time_min = 125
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeMin(100, job) end )
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Logical queue test requires --time-min to be specified and at most 100 minutes.")

        -- check that generic prioritizes Request response
        job.Request = {}
        job.Record = {}
        job.Request.time_min = 75
        job.Record.time_min = 125
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeMin(100, job) end )
        lu.assertEquals(status, true)
    end

    function TestLogicalQueue:test_match_MaximumTimeLimit()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")
        queue1.Apply = {}
        queue1.Apply.ExecutionPartition = 'debug'

        -- set partition max
        local part_list = {}
        part_list.debug = {}
        part_list.debug.max_time = 30

        local job_req = {}
        local job_rec = {}
        local job = _setup_job(job_req, job_rec, part_list)
        -- assure unspecified time_limit fails
        job.Request = {}
        job.Record = {}
        lu.assertEquals(queue1:match_RecordMaximumTimeLimit(20, job), false)
        lu.assertEquals(queue1:match_RequestMaximumTimeLimit(20, job), false)

        -- check if partition default works
        job.Request = {}
        job.Record = {}
        lu.assertEquals(queue1:match_MaximumTimeLimit(20, job), false)
        lu.assertEquals(queue1:match_MaximumTimeLimit(30, job), true)

        -- check request, and that for generic request is prioritized
        job.Request = {}
        job.Record = {}
        job.Request.time_limit = 15
        lu.assertEquals(queue1:match_RecordMaximumTimeLimit(20, job), false)
        lu.assertEquals(queue1:match_RequestMaximumTimeLimit(20, job), true)
        lu.assertEquals(queue1:match_MaximumTimeLimit(20, job), true)

        -- check record, and that for generic record is prioritized
        job.Request = {}
        job.Record = {}
        job.Record.time_limit = 15
        lu.assertEquals(queue1:match_RecordMaximumTimeLimit(20, job), true)
        lu.assertEquals(queue1:match_RequestMaximumTimeLimit(20, job), false)
        lu.assertEquals(queue1:match_MaximumTimeLimit(20, job), true)

        -- check record and request, and that for generic request is prioritized
        job.Request = {}
        job.Record = {}
        job.Record.time_limit = 25
        job.Request.time_limit = 15
        lu.assertEquals(queue1:match_RecordMaximumTimeLimit(20, job), false)
        lu.assertEquals(queue1:match_RequestMaximumTimeLimit(20, job), true)
        lu.assertEquals(queue1:match_MaximumTimeLimit(20, job), true)

    end

    function TestLogicalQueue:test_validate_RequireMaximumTimeLimit()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = {}
        queue1 = jsl.LogicalQueue:new(queue1, "test")
        queue1.Apply = {}
        queue1.Apply.ExecutionPartition = 'debug'

        -- set partition max
        local part_list = {}
        part_list.debug = {}
        part_list.debug.max_time = 100

        local job_req = {}
        local job_rec = {}
        local job = _setup_job(job_req, job_rec, part_list)
        -- assure unspecified time_limit succeeds using default
        job.Request = {}
        job.Record = {}
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, true)

        -- check that Request-specific and generic can see request time
        job.Request = {}
        job.Record = {}
        job.Request.time_limit = 50
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, true)

        job.Request.time_limit = 150
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Logical queue test requires --time to be at most 100 minutes.")

        -- check that Record-specific and generic can see record time
        job.Request = {}
        job.Record = {}
        job.Record.time_limit = 50
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, true)

        -- check that time_min can be exceeded
        job.Request = {}
        job.Record = {}
        job.Record.time_limit = 125
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Logical queue test requires --time to be at most 100 minutes.")

        -- check that generic prioritizes Request response
        job.Request = {}
        job.Record = {}
        job.Request.time_limit = 75
        job.Record.time_limit = 125
        local status,err = pcall( function () queue1:validate_RequireMaximumTimeLimit(100, job) end )
        lu.assertEquals(status, true)
    end


    function TestLogicalQueue:test_isNodeExclusive()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = jsl.LogicalQueue:new({}, "test")
        queue1.Apply.ExecutionPartition = "debug"

        local partlist = {}
        partlist.debug = {}
        partlist.debug.max_share = 0

        lu.assertEquals(queue1:isNodeExclusive(partlist), true)

        partlist.debug.max_share = 1
        lu.assertEquals(queue1:isNodeExclusive(partlist), false)
    end

    function TestLogicalQueue:test_getExecutionPartition()
        local nersc = jsl.setupVariables({}, true)
        local queue1 = jsl.LogicalQueue:new({}, "test")

        -- test explicit with no default
        queue1.Apply = {}
        queue1.Apply.ExecutionPartition = "debug"
        lu.assertEquals(queue1:getExecutionPartition(), "debug")

        -- test default partition
        nersc.queue_defaults = {}
        nersc.queue_defaults.Apply = {}
        nersc.queue_defaults.Apply.ExecutionPartition = "test_default"

        local queue2 = jsl.LogicalQueue:new({}, "test2")
        lu.assertEquals(queue2:getExecutionPartition(), "test_default")

        -- test that explicit definition supersedes default
        lu.assertEquals(queue1:getExecutionPartition(), "debug")

        -- test that we get an error
        nersc = jsl.setupVariables({}, true)
        local status,err = pcall( function () queue2:getExecutionPartition() end)
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Selected queue test2 definition has no execution partition")
    end
-- class TestLogicalQueue

TestStringMatching = {} -- class
    function TestStringMatching:test_findString()
        local nersc = jsl.setupVariables({}, true)
        local ret = false
        local q = jsl.LogicalQueue:new({}, "test")

        -- check basic string
        ret = q._findString("test", "test")
        lu.assertEquals(ret, true)

        ret = q._findString(nil, "test")
        lu.assertEquals(ret, false)

        ret = q._findString(nil, "None")
        lu.assertEquals(ret, true)

        -- check table values
        local tab1 = {}
        table.insert(tab1, "a")
        table.insert(tab1, "b")
        table.insert(tab1, "c")
        table.insert(tab1, "d")
        ret = q._findString("test", tab1)
        lu.assertEquals(ret, false)

        ret = q._findString("c", tab1)
        lu.assertEquals(ret, true)

        ret = q._findString(nil, tab1)
        lu.assertEquals(ret, false)

        table.insert(tab1, "None")
        ret = q._findString(nil, tab1)
        lu.assertEquals(ret, true)

        local tab2 = {}
        tab2['key1'] = 'a'
        tab2['key2'] = 'b'
        tab2['key3'] = 'c'
        tab2['key4'] = 'd'
        ret = q._findString("test", tab2)
        lu.assertEquals(ret, false)

        ret = q._findString("c", tab2)
        lu.assertEquals(ret, true)

        ret = q._findString(nil, tab2)
        lu.assertEquals(ret, false)

        tab2['key5'] = 'None'
        ret = q._findString(nil, tab2)
        lu.assertEquals(ret, true)

        -- if the targets are nil, findString is meant to return false
        lu.assertEquals(q._findString(nil, nil), false)
        lu.assertEquals(q._findString("test", nil), false)
    end

    function TestStringMatching:test_matchString()
        local nersc = jsl.setupVariables({}, true)
        local ret = false
        local q = jsl.LogicalQueue:new({}, "test")

        -- check basic string
        ret = q._matchString("test", "test")
        lu.assertEquals(ret, true)

        ret = q._matchString(nil, "test")
        lu.assertEquals(ret, false)

        ret = q._matchString(nil, "None")
        lu.assertEquals(ret, true)

        -- check table values
        local tab1 = {}
        table.insert(tab1, "a")
        table.insert(tab1, "b")
        table.insert(tab1, "c")
        table.insert(tab1, "d")
        ret = q._matchString("test", tab1)
        lu.assertEquals(ret, false)

        ret = q._matchString("c", tab1)
        lu.assertEquals(ret, true)

        ret = q._matchString(nil, tab1)
        lu.assertEquals(ret, false)

        table.insert(tab1, "None")
        ret = q._matchString(nil, tab1)
        lu.assertEquals(ret, true)

        local tab2 = {}
        tab2['key1'] = 'a'
        tab2['key2'] = 'b'
        tab2['key3'] = 'c'
        tab2['key4'] = 'd'
        ret = q._matchString("test", tab2)
        lu.assertEquals(ret, false)

        ret = q._matchString("c", tab2)
        lu.assertEquals(ret, true)

        ret = q._matchString(nil, tab2)
        lu.assertEquals(ret, false)

        tab2['key5'] = 'None'
        ret = q._matchString(nil, tab2)
        lu.assertEquals(ret, true)

        lu.assertEquals(q._matchString("test", "test.*"), true)

        -- if the targets are nil, q._matchString is meant to return false
        lu.assertEquals(q._matchString(nil, nil), false)
        lu.assertEquals(q._matchString("test", nil), false)
    end
-- class TestStringMatching

TestDerived = {} -- class
    function TestDerived:test__init_Account()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}

        local job = _setup_job(job_req, job_rec, part_list)

        job_req.account = "test"
        job_req.default_account = nil
        job_rec.account = nil
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test")

        job_req.account = nil
        job_req.default_account = nil
        job_rec.account = "test_record"
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test_record")

        job_req.account = "test_request"
        job_rec.account = "test_record"
        job_req.default_account = nil
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test_request")

        job_req.account = "test_request"
        job_rec.account = "test_record"
        job_req.default_account = "test_default"
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test_request")

        job_req.account = nil
        job_rec.account = "test_record"
        job_req.default_account = "test_default"
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test_record")

        job_req.account = nil
        job_rec.account = nil
        job_req.default_account = "test_default"
        job.Account = nil
        job:_init_Account()
        lu.assertEquals(job.Account, "test_default")

        job_req.account = nil
        job_rec.account = nil
        job_req.default_account = nil
        job.Account = nil
        local status,err = pcall( function () job:_init_Account() end )
        lu.assertEquals(status, false)
        lu.assertEquals(job.Account, nil)
        lu.assertEquals(tostring(err), "ERROR: Unable to determine account name.  Please resubmit your job specifying account with -A.")
    end

    function TestDerived:test__init_ArrayTasks()
        local nersc = jsl.setupVariables({}, true)
        local job_request = {}
        local job_record = {}
        local part_list = {}

        local job = _setup_job(job_request, job_record, part_list)

        job_request.array_inx = "1-10"
        job_record.array_task_cnt = nil
        job:_init_ArrayTasks()
        lu.assertEquals(job.PendingArrayTasks, 10)

        job_request.array_inx = nil
        job_record.array_task_cnt = nil
        job:_init_ArrayTasks()
        lu.assertEquals(job.PendingArrayTasks, 1)

        job_request.array_inx = nil
        job_record.array_task_cnt = 5
        job:_init_ArrayTasks()
        lu.assertEquals(job.PendingArrayTasks, 5)

        job_request.array_inx = "1-10"
        job_record.array_task_cnt = 5
        job:_init_ArrayTasks()
        lu.assertEquals(job.PendingArrayTasks, 10)
    end

    function TestDerived:test__init_Balances()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)
        nersc.nerscSlurmPluginDir = "."

        job.Username = "poorUser"
        job.Account = "poorAccount"

        local timer = os.clock()
        local count = 1
        for i = 1,count do
        job:_init_Balances()
        lu.assertEquals(job.UserBalance, 5)
        lu.assertEquals(job.AccountBalance, 5)
        end
        timer = (os.clock() - timer) / count
        print("time to init balances (fork/exec python): ", timer)

        job.UserBalance = nil
        job.AccountBalance = nil
        job.Account = "poorAccount"
        job.Username = nil
        job_req.user_name = "poorUser"
        job:_init_Balances()
        lu.assertEquals(job.UserBalance, 5)
        lu.assertEquals(job.AccountBalance, 5)

        job.UserBalance = nil
        job.AccountBalance = nil
        job.Account = nil
        job_req.account = "poorAccount"
        job.Username = nil
        job_req.user_name = "poorUser"
        job:_init_Balances()
        lu.assertEquals(job.UserBalance, 5)
        lu.assertEquals(job.AccountBalance, 5)

        job.UserBalance = nil
        job.AccountBalance = nil
        job.Account = "invalid"
        job.Username = "poorUser"
        job:_init_Balances()
        lu.assertEquals(job.UserBalance, nil)
        lu.assertEquals(job.AccountBalance, nil)
    end

    function TestDerived:test__init_NodeClass()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        nersc.arch = {}
        nersc.arch.haswell = {}
        nersc.arch.haswell.enabled = {}

        job_req.features = "haswell"
        local status,err = pcall( function() job:_init_NodeClass() end )
        if not status then
            print(tostring(err))
        end

        lu.assertEquals(status, true)
        lu.assertEquals(job.NodeClass, "haswell")
        lu.assertEquals(job.ToWrite.features, "haswell")

        job.NodeClass = nil
        job.NodeClassFeatures = nil
        job.ToWrite = {}
        job_req.features = "knl"
        status,err = pcall( function() job:_init_NodeClass() end )
        -- TODO fix this test, issue is that not matching hardware no longer rejects job
        -- lu.assertEquals(status, false)
        --lu.assertEquals(tostring(err), "ERROR: Unable to determine hardware architecture.")
    end

    function TestDerived:test__init_Username()
        local nersc = jsl.setupVariables({}, true)
        local job_request = {}
        local job_record = {}
        local part_list = {}

        local job = _setup_job(job_request, job_record, part_list)

        job.errors = {}

        job_request.user_name = nil
        local status,err = pcall( function() job:_init_Username() end )
        lu.assertEquals(status, false)
        lu.assertEquals(tostring(err), "ERROR: Failed to lookup username.  Please try again later, something appears to be wrong.")
        lu.assertEquals(job.Username, nil)

        job.errors = {}
        job_request.user_name = "testuser"
        job:_init_Username()
        lu.assertEquals(job.Username, "testuser")
    end

    function TestDerived:test_getNodes()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        local min,max = job:getNodes()
        lu.assertEquals(min, nil)
        lu.assertEquals(max, nil)

        job_req.min_nodes = 100
        job_req.max_nodes = 105
        min,max = job:getNodes()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 105)

        job_req.min_nodes = 100
        job_req.max_nodes = slurm.NO_VAL
        min,max = job:getNodes()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 100)

        job_req.min_nodes = 100
        job_req.max_nodes = 0
        min,max = job:getNodes()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 100)

        job_req.min_nodes = slurm.NO_VAL
        job_req.max_nodes = slurm.NO_VAL
        job_rec.min_nodes = 100
        job_rec.max_nodes = 105
        min,max = job:getNodes()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 105)

        job_req.min_nodes = 50
        job_req.max_nodes = 55
        job_rec.min_nodes = 100
        job_rec.max_nodes = 105
        min,max = job:getNodes()
        lu.assertEquals(min, 50)
        lu.assertEquals(max, 55)

        job.ToWrite.min_nodes = 20
        job.ToWrite.max_nodes = 25
        job_req.min_nodes = 50
        job_req.max_nodes = 55
        job_rec.min_nodes = 100
        job_rec.max_nodes = 105
        min,max = job:getNodes()
        lu.assertEquals(min, 20)
        lu.assertEquals(max, 25)
    end

    function TestDerived:test_getCpus()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        local min,max = job:getCpus()
        lu.assertEquals(min, nil)
        lu.assertEquals(max, nil)

        job_req.min_cpus = 100
        job_req.max_cpus = 105
        min,max = job:getCpus()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 105)

        job_req.min_cpus = 100
        job_req.max_cpus = slurm.NO_VAL
        min,max = job:getCpus()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 100)

        job_req.min_cpus = 100
        job_req.max_cpus = 0
        min,max = job:getCpus()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 100)

        job_req.min_cpus = slurm.NO_VAL
        job_req.max_cpus = slurm.NO_VAL
        job_rec.min_cpus = 100
        job_rec.max_cpus = 105
        min,max = job:getCpus()
        lu.assertEquals(min, 100)
        lu.assertEquals(max, 105)

        job_req.min_cpus = 50
        job_req.max_cpus = 55
        job_rec.min_cpus = 100
        job_rec.max_cpus = 105
        min,max = job:getCpus()
        lu.assertEquals(min, 50)
        lu.assertEquals(max, 55)

        job.ToWrite.min_cpus = 20
        job.ToWrite.max_cpus = 25
        job_req.min_cpus = 50
        job_req.max_cpus = 55
        job_rec.min_cpus = 100
        job_rec.max_cpus = 105
        min,max = job:getCpus()
        lu.assertEquals(min, 20)
        lu.assertEquals(max, 25)
    end

    function TestDerived:test_getArchChargeFactor()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        nersc.default_arch = "haswell"
        nersc.arch = {}
        nersc.arch.knl = {}
        nersc.arch.knl.chargeFactor = 0.5
        nersc.arch.haswell = {}
        nersc.arch.haswell.chargeFactor = 2.0
        nersc.arch.ivybridge = {}
        -- intentionally no charge factor defined for ivybridge here

        job.NodeClass = "knl"
        local chargeFactor = job:getArchChargeFactor()
        lu.assertEquals(chargeFactor, 0.5)

        job.NodeClass = nil
        chargeFactor = job:getArchChargeFactor()
        lu.assertEquals(chargeFactor, 2.0)

        job.NodeClass = "ivybridge"
        chargeFactor = job:getArchChargeFactor()
        lu.assertEquals(chargeFactor, 1)
    end

    function TestDerived:test_getTimeLimit()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)


        local queue1 = jsl.LogicalQueue:new({}, "test")
        queue1.Apply.ExecutionPartition = 'debug'

        local time = nil
        local status,err = pcall( function () time = job:getTimeLimit(queue1) end )
        if not status then
            print(tostring(err))
        end
        lu.assertEquals(status, true)
        lu.assertNil(time)

        -- set partition max
        part_list.debug = {}
        part_list.debug.max_time = 30

        status,err = pcall( function () time = job:getTimeLimit(queue1) end )
        lu.assertTrue(status)
        lu.assertEquals(time, 30)

        part_list.debug.default_time = 5
        status,err = pcall( function () time = job:getTimeLimit(queue1) end )
        lu.assertTrue(status)
        lu.assertEquals(time, 5)

        job_rec.time_limit = 28
        status,err = pcall( function () time = job:getTimeLimit(queue1) end )
        lu.assertTrue(status)
        lu.assertEquals(time, 28)

        job_req.time_limit = 25
        status,err = pcall( function () time = job:getTimeLimit(queue1) end )
        lu.assertTrue(status)
        lu.assertEquals(time, 25)
    end

    function TestDerived:test_estimateCost()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        job_req.cpus_per_task = slurm.NO_VAL16
        job_req.ntasks_per_core = slurm.NO_VAL16
        job_req.ntasks_per_socket = slurm.NO_VAL16
        job_req.ntasks_per_node = slurm.NO_VAL16

        local queue = jsl.LogicalQueue:new({}, "debug")
        queue.Apply.ExecutionPartition = "debug"
        part_list.debug = {}
        part_list.debug.default_time = 5
        part_list.debug.max_time = 30
        part_list.debug.max_share = 0

        nersc.machineChargeFactor = 1.0
        nersc.arch = {}
        nersc.arch.haswell = {}
        nersc.arch.haswell.enabled = true
        nersc.arch.haswell.cpus_per_core = 2
        nersc.arch.haswell.cores_per_socket = 16
        nersc.arch.haswell.sockets_per_node = 2
        nersc.arch.haswell.default_cpus_per_task = 2
        nersc.default_arch = "haswell"

        -- partition default time of 5 minutes
        -- partition default of a single node
        -- all charge factors 1.0
        local corehours = job:estimateCost(queue)
        lu.assertEquals(corehours * 60, 160)

        -- partition default time of 5 minutes
        -- partition default of a single node
        -- machine CF 2.0, others 1.0
        nersc.machineChargeFactor = 2.0
        corehours = job:estimateCost(queue)
        lu.assertEquals(corehours * 60, 320)

        -- 10 nodes
        job_rec.min_nodes = 10
        corehours = job:estimateCost(queue)
        -- complicated way of dealing with numerical imprecision
        lu.assertEquals(math.floor(corehours * 60 - 3200 + 0.5), 0)

        -- for 1 hour
        job_rec.time_limit = 60
        corehours = job:estimateCost(queue)
        lu.assertEquals(corehours * 60, 38400)

        -- arch charge factor of 0.1
        nersc.arch.haswell.chargeFactor = 0.1
        corehours = job:estimateCost(queue)
        lu.assertEquals(corehours * 60, 3840)

        -- queue charge factor of 200
        queue.chargeFactor = 200
        corehours = job:estimateCost(queue)
        lu.assertEquals(corehours * 60, 768000)
    end

    function TestDerived:test__calculate_core_resources()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        job_req.cpus_per_task = slurm.NO_VAL16
        job_req.ntasks_per_core = slurm.NO_VAL16
        job_req.ntasks_per_socket = slurm.NO_VAL16
        job_req.ntasks_per_node = slurm.NO_VAL16

        local queue = jsl.LogicalQueue:new({}, "debug")
        queue.Apply.ExecutionPartition = "debug"
        part_list.debug = {}
        part_list.debug.default_time = 5
        part_list.debug.max_time = 30
        part_list.debug.max_share = 0

        local sharedq = jsl.LogicalQueue:new({}, "shared")
        sharedq.Apply.ExecutionPartition = "shared"
        part_list.shared = {}
        part_list.shared.default_time = 5
        part_list.shared.max_time = 720
        part_list.shared.max_share = 1
        part_list.shared.def_mem_per_cpu = 1952 * 1024 * 1024

        nersc.arch = {}
        nersc.arch.haswell = {}
        nersc.arch.haswell.enabled = true
        nersc.arch.haswell.cpus_per_core = 2
        nersc.arch.haswell.cores_per_socket = 16
        nersc.arch.haswell.sockets_per_node = 2
        nersc.arch.haswell.default_cpus_per_task = 2
        nersc.default_arch = "haswell"

        -- with no further config, should get a full node
        local cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 32)
        local nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 1)

        -- request 2 nodes (128 cpus)
        job_req.min_nodes = 2
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 64)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 2)

        -- request 32 tasks (2 cpus each)
        job_req.min_nodes = slurm.NO_VAL
        job_req.num_tasks = 32
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 32)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 1)

        -- request 32 tasks (4 cpus each)
        job_req.min_nodes = slurm.NO_VAL
        job_req.num_tasks = 32
        job_req.cpus_per_task = 4
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 64)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 2)
        job_req.cpus_per_task = slurm.NO_VAL16

        -- request 32 tasks (2 tasks per socket)
        job_req.num_tasks = 32
        job_req.ntasks_per_socket = 2
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 256)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 8)
        job_req.ntasks_per_socket = slurm.NO_VAL16

        -- request 128 tasks (2 tasks per core)
        job_req.num_tasks = 128
        job_req.ntasks_per_core = 2
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 64)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 2)
        job_req.ntasks_per_core = slurm.NO_VAL16

        -- request 32 tasks (1 task per node)
        job_req.num_tasks = 32
        job_req.ntasks_per_node = 1
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 1024)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 32)
        job_req.ntasks_per_node = slurm.NO_VAL16
        job_req.num_tasks = slurm.NO_VAL

        -- allow node sharing
        cpus = job:_calculate_core_resources(sharedq)
        lu.assertEquals(cpus, 1)
        nodes = job:_get_effective_nodes(sharedq)
        lu.assertEquals(nodes, 1/32.)

        -- request 16 cpus
        job_req.cpus_per_task = 16
        cpus = job:_calculate_core_resources(sharedq)
        lu.assertEquals(cpus, 8)
        nodes = job:_get_effective_nodes(sharedq)
        lu.assertEquals(nodes, 0.25)
        job_req.cpus_per_task = slurm.NO_VAL16

        -- request 10G of memory
        job_req.min_mem_per_node = 10 * 1024 * 1024 * 1024
        cpus = job:_calculate_core_resources(sharedq)
        lu.assertEquals(cpus, 3)
        nodes = job:_get_effective_nodes(sharedq)
        lu.assertEquals(nodes, 3/32.)
        job_req.min_mem_per_node = nil

        -- use existing job_rec
        job_rec.min_nodes = 2
        job_rec.max_nodes = 4
        job_rec.min_cpus = 128
        job_rec.max_cpus = 256
        cpus = job:_calculate_core_resources(queue)
        lu.assertEquals(cpus, 128)
        nodes = job:_get_effective_nodes(queue)
        lu.assertEquals(nodes, 4)

        -- check validation routine
        local status,err = pcall( function () sharedq:validate_RequireMaxNode(0.1, job) end )
        lu.assertEquals(status, false)
        lu.assertEquals(err['userMsg'], "More resources requested than allowed for logical queue shared (4.0 requested core-equivalents > 0.1)")

        local status,err = pcall( function () sharedq:validate_RequireMaxNode(4, job) end )
        lu.assertEquals(status, true)

        local status,err = pcall( function () sharedq:validate_RequireMaxNode(5, job) end )
        lu.assertEquals(status, true)
    end
-- class TestDerived

TestArrayInx = {} -- class
    function TestArrayInx:test_basic()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)
        job_req.array_inx = "1-5"
        lu.assertEquals(job:_get_request_array_task_count(), 5)
        job_req.array_inx = "1-5:2"
        lu.assertEquals(job:_get_request_array_task_count(), 3)
        job_req.array_inx = "1-5:2,1-100%4"
        lu.assertEquals(job:_get_request_array_task_count(), 103)
        job_req.array_inx = "1,2,3,4,5"
        lu.assertEquals(job:_get_request_array_task_count(), 5)
        job_req.array_inx = "0-4:2"
        lu.assertEquals(job:_get_request_array_task_count(), 3)
        job_req.array_inx = "1-3,5-7:2"
        lu.assertEquals(job:_get_request_array_task_count(), 5)
    end
-- class TestArrayInx


TestNodeClass = {} -- class

    function TestNodeClass:test_KnlModes()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        -- case 0 invalid knl mode
        nersc.arch = {}
        nersc.arch.haswell = {}
        nersc.arch.haswell.enabled = true

        job_req.features = "knl,quad,cache"
        job_rec.features = nil
        node_class,features = job:_determine_node_class()
        lu.assertNil(node_class)
        lu.assertNil(features)

        -- case 1 valid knl mode, with "knl" included
        nersc.arch.knl = {}
        nersc.arch.knl.enabled = true
        nersc.arch.knl.default_numa = 'quad'
        nersc.arch.knl.default_mcdram = 'cache'

        job_req.features = "knl,quad,cache"
        job_rec.features = nil
        node_class,features,write_features = job:_determine_node_class()

        lu.assertEquals(node_class, "knl")
        lu.assertEquals(features, "quad&cache")
        lu.assertEquals(job_req.features, "knl,quad,cache")

        job_req.features = nil
        job_rec.features = "knl&quad&cache"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "knl")
        lu.assertEquals(features, "quad&cache")
        lu.assertEquals(towrite, "knl&quad&cache")

        -- case 2 invalid knl mode, with "knl" included
        job_req.features = "knl,walrus,cache"
        job_rec.features = nil
        node_class,features,towrite = job:_determine_node_class()
        lu.assertNil(node_class)
        lu.assertNil(features)

        -- case 3 just "knl" expands out knl defaults
        job_req.features = "knl"
        job_rec.features = "knl&quad&cache"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "knl")
        lu.assertEquals(features, "quad&cache")
        lu.assertEquals(towrite, "knl&quad&cache")
    end

    function TestNodeClass:test_HaswellClass()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        nersc.arch = {}
        nersc.arch.knl = {}
        nersc.arch.haswell = {}
        nersc.arch.knl.enabled = true
        nersc.arch.knl.default_numa = 'quad'
        nersc.arch.knl.default_mcdrom = 'cache'
        nersc.arch.haswell.enabled = true

        job_req.features = "haswell"
        job_rec.features = nil
        towrite = nil
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "haswell")
        lu.assertNil(features)
        lu.assertEquals(towrite, "haswell")

        job_req.features = nil
        job_rec.features = "haswell"
        towrite = nil
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "haswell")
        lu.assertNil(features)
        lu.assertEquals(towrite, "haswell")
    end

    function TestNodeClass:test3_NonKnlOrHaswellClass()
        local nersc = jsl.setupVariables({}, true)
        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)

        -- case 1, specified feature with no defaults
        nersc.arch = {}
        nersc.arch.knl = {}
        nersc.arch.haswell = {}
        nersc.arch.knl.enabled = true
        nersc.arch.knl.default_numa = 'quad'
        nersc.arch.knl.default_mcdrom = 'cache'
        nersc.arch.haswell.enabled = true
        job_req.features = "ivybridge"
        job_rec.features = nil
        node_class,features,towrite = job:_determine_node_class()
        lu.assertNil(node_class)
        lu.assertNil(features)
        lu.assertNil(towrite)

        job_req.features = nil
        job_rec.features = "ivybridge"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertNil(node_class)
        lu.assertNil(features)
        lu.assertNil(towrite)

        --- TODO THIS SEEMS WRONG, ivybridge is not enabled, and yet works!

        -- case 2, specified feature with invalid default
        job_req.features = "ivybridge"
        job_rec.features = nil
        nersc.default_arch = "ivybridge"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, nil)
        lu.assertNil(features)
        lu.assertEquals(towrite, nil)

        -- case 3, specified feature with valid default
        nersc.arch.ivybridge = {}
        nersc.arch.ivybridge.enabled = true

        job_req.features = "ivybridge"
        job_rec.features = nil
        nersc.default_arch = "ivybridge"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "ivybridge")
        lu.assertNil(features)
        lu.assertEquals(towrite, "ivybridge")

        job_req.features = nil
        job_rec.features = "ivybridge"
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "ivybridge")
        lu.assertNil(features)
        lu.assertEquals(towrite, "ivybridge")

        -- case 3, unspecified feature with no defaults
        job_req.features = nil
        job_rec.features = nil
        nersc = jsl.setupVariables({}, true)
        nersc.default_arch = "ivybridge"
        nersc.arch = {}
        nersc.arch.ivybridge = {}
        nersc.arch.ivybridge.enabled = true
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "ivybridge")
        lu.assertNil(features)
        lu.assertEquals(towrite, "ivybridge")

        job_req.features = nil
        job_rec.features = nil
        node_class,features,towrite = job:_determine_node_class()
        lu.assertEquals(node_class, "ivybridge")
        lu.assertNil(features)
        lu.assertEquals(towrite, "ivybridge")
    end
-- class TestNodeClass

TestFsLicenses = {} -- class
    function TestFsLicenses:test_basicLicenses()
        local nersc = jsl.setupVariables({}, true)
        nersc.filesystemLicenses = {
            'cscratch1','project','projecta','projectb','dna','cfs'
        }
        nersc.filesystemPatterns = {
            ['cfs'] = {'^/global/cfs/'},
            ['cscratch1'] = {'/global/cscratch1/sd'},
            ['project'] = {'^/global/project/projectdirs/','^/project/projectdirs/'},
            ['projecta'] = {'^/global/projecta/'},
            ['projectb'] = {'^/global/projectb/'},
            ['dna'] = {'^/global/dna/'}

        }

        local job_req = {}
        local job_rec = {}
        local part_list = {}
        local job = _setup_job(job_req, job_rec, part_list)
        local result

        -- case 1 users asks for cscratch1
        job_req.licenses = "cscratch1"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1")

        -- case 2 users asks for one cscratch1 license
        job_req.licenses = "cscratch1:1"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1")

        -- case 3 users asks for many cscratch1 licenses (gets one)
        job_req.licenses = "cscratch1:105"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1")

        -- case 4 user asks for multiple licenses
        job_req.licenses = "cscratch1,dna,project"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1,dna:1,project:1")

        -- case 5 user asks for one fs license and something else
        job_req.licenses = "cscratch1,ipaddr:10"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(count, 1)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1,ipaddr:10")

        -- case 6 user asks for one fs license and something else (no specified number)
        job_req.licenses = "cscratch1,ipaddr"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(count, 1)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1,ipaddr:1")

        -- case 7 implicit license through TMPDIR environment variable
        job_req.licenses = nil
        job_req.environment = {}
        job_req.environment.TMPDIR = "/global/cscratch1/sd/test"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(count, 1)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1")

        -- case 8 workdir implicit license
        job_req.licenses = "project"
        job_req.environment = nil
        job_req.work_dir = "/global/cscratch1/sd/test"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(count, 2)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1,project:1")

        -- case 9 SCRATCH environment-aided fs license
        job_req.licenses = "SCRATCH"
        job_req.environment = {}
        job_req.environment.SCRATCH = "/global/cscratch1/sd/test"
        job.ToWrite = {}
        result,count = job:_init_FilesystemLicenses(job_req, nil)
        lu.assertEquals(count, 1)
        lu.assertEquals(job.ToWrite.licenses, "cscratch1:1")
    end
-- class TestFsLicenses

local runner = lu.LuaUnit.new()
runner:setOutputType("tap")
os.exit( runner:runSuite() )
