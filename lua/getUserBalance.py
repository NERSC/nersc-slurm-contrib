#!/usr/bin/env python

import os
import sys

username = sys.argv[1]
account = sys.argv[2]

userBalance = 0.
accountBalance = 0.

if username == "richUser":
    userBalance = 50000000.
elif username == "normalUser":
    userBalance = 50000.
elif username == "poorUser":
    userBalance = 5.
elif username == "destituteUser":
    userBalance = -5.
else:
    print "Invalid username!"
    sys.exit(1)

if account == "richAccount":
    accountBalance = 50000000.
elif account == "normalAccount":
    accountBalance = 50000.
elif account == "poorAccount":
    accountBalance = 5.
elif account == "destituteAccount":
    accountBalance = -5.
else:
    print "Invalid account!"
    sys.exit(1)

print("%0.2f,%0.2f" % (userBalance, accountBalance))

