---
system: cori

queue_defaults:
  Requirements:
    IfArchKnlAllowedNumaModes: quad
    IfArchKnlAllowedMcdramModes: cache

queues:
  debug_knl:
    MatchingCriteria:
    # in the case of a no option job submission, need all blank to prevent
    # accidental matching during an update
    - RequestQos: None
      RequestPartition: None
      RecordQos: None
      RecordPartition: None
      Arch: knl

    # expected job submission (-q debug)
    - RequestQos: debug
      RequestPartition: None
      Arch: knl

    # old interface (-p debug), allow qos debug just in case
    - RequestPartition: debug
      RequestQos: [None, debug]
      Arch: knl

    # match for job update
    - RecordQos: debug_knl
      RequestQos: None
      RequestPartition: None
      Arch: knl
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: debug_knl
      ExecutionPartition: debug_knl
    EvaluationPriority: 2

  ## must match other architectures first, removed "haswell" requirement
  ## so that an error will be produced telling them to specify arch if
  ## this would otherwise match
  debug_hsw:
    MatchingCriteria:
    # in the case of a no option job submission, need all blank to prevent
    # accidental matching during an update
    - RequestQos: None
      RequestPartition: None
      RecordQos: None
      RecordPartition: None

    # expected job submission (-q debug)
    - RequestQos: debug
      RequestPartition: None

    # old interface (-p debug), allow qos debug just in case
    - RequestPartition: debug
      RequestQos: [None, debug]

    # match for job update
    - RecordQos: debug_hsw
      RequestQos: None
      RequestPartition: None

    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: debug_hsw
      ExecutionPartition: debug_hsw
    EvaluationPriority: 1

  regular_hsw:
    MatchingCriteria:
    - RequestQos: regular
      RequestPartition: None
      Arch: haswell
    - RequestPartition: regular
      RequestQos: None
      Arch: haswell
    - RecordQos: regular_1
      RequestQos: None
      Arch: haswell
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: regular_1
      ExecutionPartition: regular_hsw
    EvaluationPriority: 10

  regular_knl:
    MatchingCriteria:
    - RequestQos: regular
      RequestPartition: None
      Arch: knl
    - RequestPartition: regular
      RequestQos: None
      Arch: knl
    - RecordQos: regular_1
      RequestQos: None
      Arch: knl
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: regular_1
      ExecutionPartition: regular_knl
    EvaluationPriority: 10

  regularx_hsw:
    MatchingCriteria:
    - RequestQos: regular
      RequestPartition: None
      Arch: haswell
      MinNodes: 1400
    - RequestPartition: regular
      RequestQos: None
      Arch: haswell
      MinNodes: 1400
    - RecordQos: regular_0
      RecordPartition: regularx_hsw
      RequestQos: None
      RequestPartition: None
      Arch: haswell
      MinNodes: 1400
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: regular_0
      ExecutionPartition: regularx_hsw
    EvaluationPriority: 100

  regularx_knl:
    MatchingCriteria:
    - RequestQos: regular
      RequestPartition: None
      Arch: knl
      MinNodes: 8192
    - RequestPartition: regular
      RequestQos: None
      Arch: knl
      MinNodes: 8192
    - RecordQos: regular_0
      RecordPartition: regularx_knl
      RequestQos: None
      RequestPartition: None
      Arch: knl
      MinNodes: 8192
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: regular_0
      ExecutionPartition: regularx_knl
    EvaluationPriority: 101
    chargeFactor: 0.5

  regular_knl_bigjob_discount:
    MatchingCriteria:
    - RequestQos: regular
      RequestPartition: None
      MinNodes: 1024
      Arch: knl
    - RequestPartition: regular
      RequestQos: None
      MinNodes: 1024
      Arch: knl
    - RecordQos: regular_0
      RecordPartition: regular_knl
      RequestQos: None
      MinNodes: 1024
      RequestPartition: None
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: regular_0
      ExecutionPartition: regular_knl
    EvaluationPriority: 100
    chargeFactor: 0.5

  flex_knl:
    MatchingCriteria:
    - RequestQos: flex
      Arch: knl
      MaximumTimeMin: 120
      MinimumTimeLimit: 121
      MaxNodes: 256
    - RecordQos: flex
      RequestQos: None
      Arch: knl
      MaximumTimeMin: 120
      MinimumTimeLimit: 121
      MaxNodes: 256
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: flex
      ExecutionPartition: regular_knl
    EvaluationPriority: 100
    chargeFactor: 0.25

# Resurrect "low" QOS for KNL for queue committee
  low_knl:
    MatchingCriteria:
# If user request just "low" and KNL arch
    - RequestQos: low
      RequestPartition: None
      Arch: knl
# If users requests "low" and the regular partition
    - RequestPartition: regular
      RequestQos: low
      Arch: knl
# This matches job update requests for the "low" QOS for KNL
    - RecordQos: low_knl
      RecordPartition: regular_knl
      RequestQos: None
      RequestPartition: None
      Arch: knl
    Requirements:
# User must have specified the architecture to access this QOS
      RequireArchSpecified: true
    Apply:
# Ensure the job runs in the "low_knl" QOS in the "regular" partition
      ExecutionQos: low_knl
      ExecutionPartition: regular_knl
    EvaluationPriority: 100
# Charge rate is 50% the usual KNL rate
    chargeFactor: 0.5

  premium_hsw:
    MatchingCriteria:
    - RequestQos: premium
      RequestPartition: None
      Arch: haswell
    - RequestPartition: regular
      RequestQos: premium
      Arch: haswell
    - RecordQos: premium
      RequestQos: None
      RequestPartition: None
      Arch: haswell
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: premium
      ExecutionPartition: regular_hsw
    EvaluationPriority: 10
    chargeFactor: 2

  premium_knl:
    MatchingCriteria:
    - RequestQos: premium
      RequestPartition: None
      Arch: knl
    - RequestPartition: regular
      RequestQos: premium
      Arch: knl
    - RecordQos: premium
      RequestQos: None
      RequestPartition: None
      Arch: knl
    Requirements:
      RequireArchSpecified: true
    Apply:
      ExecutionQos: premium
      ExecutionPartition: regular_knl
    EvaluationPriority: 10
    chargeFactor: 2

  shared:
    MatchingCriteria:
    - RequestQos: shared
      RequestPartition: None
    - RequestPartition: shared
      RequestQos: None
    - RecordQos: shared
      RequestQos: None
      RequestPartition: None
    Requirements:
      RequireMaxCpuPerNodeFraction: 0.5
    Apply:
      ExecutionArch: haswell
      ExecutionQos: shared
      ExecutionPartition: shared
      ExecutionGres: craynetwork:0
    EvaluationPriority: 50

  reservation:
    MatchingCriteria:
    - RequestAdvancedReservation: true
    - RecordAdvancedReservation: true
    Requirements:
      RequireArchSpecified: true
      IfArchKnlAllowedNumaModes: [quad,snc4]
      IfArchKnlAllowedMcdramModes: [cache,flat]
    Apply:
      ExecutionQos: resv
      ExecutionPartition: resv
    EvaluationPriority: 2500

  reservation_shared:
    MatchingCriteria:
    - RequestAdvancedReservation: true
      RequestQos: shared
    - RecordAdvancedReservation: true
      RequestQos: shared
    - RecordAdvancedReservation: true
      RecordQos: resv_shared
    Requirements:
      RequireMaxCpuPerNodeFraction: 0.5
    Apply:
      ExecutionArch: haswell
      ExecutionQos: resv_shared
      ExecutionPartition: resv_shared
      ExecutionGres: craynetwork:0
    EvaluationPriority: 2501

  interactive:
    MatchingCriteria:
    - RequestQos: interactive
      RequestPartition: None
    - RecordQos: interactive
      RequestQos: None
      RequestPartition: None
    Requirements:
      RequireInteractive: true
      RequireArchSpecified: true
    Apply:
      ExecutionQos: interactive
      ExecutionPartition: interactive
    EvaluationPriority: 1000

  system:
    MatchingCriteria:
    - RequestQos: system
      RequestPartition: None
    - RequestQos: None
      RequestPartition: system
    - RecordQos: system
      RequestQos: None
      RequestPartition: None
    Apply:
      ExecutionQos: system
      ExecutionPartition: system
    EvaluationPriority: 20000
    RequireNimBalance: false

arch:
  haswell:
    enabled: true
    cpus_per_core: 2
    cores_per_socket: 16
    sockets_per_node: 2
    # chargeFactor = 64 cpus/node *  2.1875 NERSC-Hours/cpu = 140 NERSC-Hours/node
    chargeFactor: 2.1875
  knl:
    enabled: true
    cpus_per_core: 4
    cores_per_socket: 68
    sockets_per_node: 1
    default_numa: 'quad'
    default_mcdram: 'cache'
    # chargeFactor = 272 cpus/node * 0.294117647058824 NERSC-Hours/cpu = 80 NERSC-Hours/node
    chargeFactor: 0.294117647058824

filesystemLicenses: ['cfs','cscratch1','project','projecta','projectb','dna','seqfs','cvmfs']
filesystemPatterns:
  cscratch1: [ '/global/cscratch1/sd' ]
  project: ['^/global/project/projectdirs/','^/project/projectdirs/' ]
  projecta: ['^/global/projecta/']
  projectb: ['^/global/projectb/']
  dna: ['^/global/dna/']
  seqfs: ['^/global/seqfs/']
  cfs: ['^/global/cfs/']
  cvmfs: ['^/cvmfs/', '^/cvmfs_nfs/']
