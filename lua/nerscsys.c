/*
 * ‘NERSC Slurm Software Configurations and Plugins’ Copyright(c) 2018-2020,
 * The Regents of the University of California, through Lawrence Berkeley
 * National Laboratory (subject to receipt of any required approvals from the
 * U.S. Dept. of Energy). All rights reserved.
 *
 * If you have questions about your rights to use or distribute this software,
 * please contact Berkeley Lab's Intellectual Property Office at IPO@lbl.gov.
 *
 * NOTICE.  This Software was developed under funding from the U.S. Department
 * of Energy and the U.S. Government consequently retains certain rights. As
 * such, the U.S. Government has been granted for itself and others acting on
 * its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
 * Software to reproduce, distribute copies to the public, prepare derivative
 * works, and perform publicly and display publicly, and to permit other to do 
 * so. 
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * (1) Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 * (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * (3) Neither the name of the University of California, Lawrence Berkeley
 *     National Laboratory, U.S. Dept. of Energy, nor the names of its
 *     contributors may be used to endorse or promote products derived from this
 *     software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * You are under no obligation whatsoever to provide any bug fixes, patches, or
 * upgrades to the features, functionality or performance of the source code
 * ("Enhancements") to anyone; however, if you choose to make your Enhancements
 * available either publicly, or directly to Lawrence Berkeley National
 * Laboratory, without imposing a separate written license agreement for such
 * Enhancements, then you hereby grant the following license: a  non-exclusive,
 * royalty-free perpetual license to install, use, modify, prepare derivative 
 * works, incorporate into other computer software, distribute, and sublicense
 * such enhancements or derivative works thereof, in binary and source code
 * form.
 */

#define _GNU_SOURCE

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <sys/file.h>

int _sleep(lua_State *L);
int _flock(lua_State *L);
int _timestr_iso8601(lua_State *L);

static const luaL_Reg nersc_routines[] =
{
        { "sleep",	&_sleep },
        { "flock",	&_flock },
	{ "timestr_iso8601",&_timestr_iso8601 },
	{ NULL, NULL }
};

int luaopen_nerscsys(lua_State *L)
{
	luaL_newlibtable(L, nersc_routines);
	luaL_setfuncs(L, nersc_routines, 0);
	return 1;
}

int _sleep(lua_State *L)
{
	double sleepwait = luaL_optnumber(L, 1, FP_NAN);
	double sec, nsec;
	struct timespec ts;

	if (signbit(sleepwait) || isnan(sleepwait) || isinf(sleepwait))
		return 1;

	nsec = modf(sleepwait, &sec);
	nsec = ceil(nsec * 1000000000);
	if (nsec >= 1000000000) {
		sec++;
		nsec = 0;
	}

	ts.tv_sec = sec;
	ts.tv_nsec = nsec;

	nanosleep(&ts, NULL);
	return 1;
}

int _flock(lua_State *L)
{
	luaL_Stream *fh;
	int fd = -1;

	if ((fh = luaL_testudata(L, 1, LUA_FILEHANDLE)))
		fd = fileno(fh->f);
	else if (lua_type(L, 1) == LUA_TNUMBER)
		fd = lua_tointeger(L, 1);

	if (fd >= 0)
		lua_pushinteger(L, flock(fd, LOCK_EX));
	return 1;
}

int _timestr_iso8601(lua_State *L)
{
	time_t curr = time(NULL);
	struct tm currtm;
	int gmtoff_hour = 0;
	int gmtoff_min = 0;
	int tmp = 0;
	char *out = NULL;

	localtime_r(&curr, &currtm);
	tmp = currtm.tm_gmtoff / 60;
	gmtoff_hour = tmp / 60;
	gmtoff_min = tmp % 60;
	
	if (asprintf(&out, "%04d-%02d-%02dT%02d:%02d:%02d%+03d:%02d",
			currtm.tm_year + 1900, currtm.tm_mon + 1,
			currtm.tm_mday, currtm.tm_hour, currtm.tm_min,
			currtm.tm_sec, gmtoff_hour, gmtoff_min) > 0)
	{
		lua_pushstring(L, out);
		free(out);
	} else
		lua_pushnil(L);
	return 1;
}
